PRINT_VARS = False
load('mcs.sage')

COUNT = 5
DIM = [25*i for i in range(1, 17)]
SIGMA = 1

embeddingAttackTimeSeries = DataSeries("Time for Embedding Attack")
reductionAttackTimeSeries = DataSeries("Time for Basis Reduction Attack")

embeddingAttackSuccessRate = DataSeries("Success Rate for Embedding Attack")

def statistics():
    for N in DIM:
        print("Dimension: " + str(N))
        sys.stdout.flush()

        successRate = 0
        timeEmbedding = []
        for _ in range(COUNT):
            
            M = MCS.Micciancio(N, SIGMA)
            K = M.keygen()
            B = K["B"]
            s = 3
        
            A = MCSAttack(N, B, s)
            
            m = random_vector(ZZ, N, x=-s, y=s+1)
            c = M.encrypt(m)
            m2 = A.embeddingAttack(c, timeEmbedding, domain=range(-s, s+1))
            if(m == m2):
                successRate += 1
            sys.stdout.flush()
    
        embeddingAttackTimeSeries.addPoint(N, round(mean(timeEmbedding)))
        embeddingAttackSuccessRate.addPoint(N, successRate)

        print("  success-rate: " + str(successRate))
        print("  current mean time: " + str(round(mean(timeEmbedding)/1000)) + "sec")
        sys.stdout.flush()

def print_statistics():
    print("\n *** Attack Statistics *** \n")

    print(embeddingAttackTimeSeries.print_series())
    print(embeddingAttackSuccessRate.print_series())

    sys.stdout.flush()

try:
    statistics()
except (KeyboardInterrupt, SystemExit):
    pass
finally:
    print_statistics()
load('ggh.sage')

N = 200
SIGMA = 3

file = "ggh_challenges/dim" + str(N) + ".sage"
load(file)

BB = matrix(ZZ, N, B)
cc = vector(ZZ, N, c)

"""
G = GGH.GGHParams(N, SIGMA)
K = G.keygen()
B = K["B"]
R = G.getPrivateKey()

m = random_vector(ZZ, N, x=-SIGMA, y=SIGMA+1)
c = G.encrypt(m)
"""

A = GGHAttack(N, BB, SIGMA)
m1 = A.nguyenAttack(cc)

print(m1)
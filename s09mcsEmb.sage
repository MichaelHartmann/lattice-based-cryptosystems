################################################################################################################################
# Statistic 09
#
# - How long does it take to attack MCS with Embedding Attack
# - What are the block sizes to successfully attack it
#
################################################################################################################################

import sys

PRINT_VARS = False

load('mcs.sage')

attackTimes = DataSeries("Time for Embedding Attack")
blockSize = DataSeries("Used Block Size")
fails = DataSeries("fails")


def mcs_attack_statistics(x=1, y=33):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    
    tt = 0
    r = 0
    for N in DIMENSIONS:
        r += 1
        timeTotal = Timer("Total")
        timeTotal.start()
        sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
        sys.stdout.flush()
            
        M = MCS.Micciancio(N)
        k = M.keygen()
        pk = k["B"]
        
        A = MCSAttack(N, pk, 1)
        
        m = random_vector(ZZ, N, x=-1, y=2)
        c = M.encrypt(m)
        
        t = []
        n = []
        m2 = A.embeddingAttack(c, t=t, neededBlockSize=n)
        attackTimes.addPoint(N, t[-1])
        blockSize.addPoint(N, n[-1])
        
        print(m == m2)
        
        if(m2 != m):
            fails.addPoint(N, 1)
        
        save_statistics("s09mcsEmb_" + str(x) + "_" + str(y) + ".sage")
        tt = timeTotal.end()

def str_statistics():
    text = "#"
    text += "\n# *** s09 MCS attacks  *** \n"
    text += attackTimes.print_series()
    text += blockSize.print_series()
    text += fails.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type mcs_attack_statistics(x, y), where x is the start value (times 25), and y-1 is the end value (times 25)")
load('ggh.sage')

SYSTEMS = [GGH.FirstAttempt, GGH.GGHParams, GGH.GGHTriagonal, GGH.Micciancio]
DIMENSIONS = [25*i for i in range(1,3)]
COUNT = 10
SIGMA = 1

i = 0
for system in SYSTEMS:
    i += 1
    print("System: " + str(i))
    for N in DIMENSIONS:
        print("  dimension: " + str(N))
        
        avgCount = 0
        avgPriTime = 0
        avgPubTime = 0
        for _ in range(COUNT):
            s = system(N, SIGMA)
            k = s.keygen()
            stat = k['statistics']
            avgCount += stat['iterations']
            avgPriTime += stat['private']
            avgPubTime += stat['public']

        print("    avg iterations:           " + str(round(avgCount/COUNT)))
        print("    avg time for private key: " + str(round(avgPriTime/COUNT)))
        print("    avg time for public key:  " + str(round(avgPubTime/COUNT)))
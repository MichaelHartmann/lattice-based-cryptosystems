################################################################################################################################
# Statistic 01
#
# - How long takes it to compute the inverses for finding sigma
#   - Is there a difference if the basis is reduced or not
# - How long takes it to reduce the basis of the private key
# - Is there a difference in the quality of the basis (-> larger sigma) if the basis is reduced
#
################################################################################################################################



import sys

PRINT_VARS = False

load('definitions.sage')

invertingTimes = DataSeries("Time for inverting non-reduced private key")
invertingTimesR = DataSeries("Time for inverting reduced private key")

lllTimings = DataSeries("Time for reducing private key")

sigmaValues = DataSeries("Value of sigma for non-reduced private key")
sigmaValuesR = DataSeries("Value of sigma for reduced private key")

DIMENSIONS = [i*25 for i in range(1, 27)]
COUNT = 10

def compute_s(R, method="inverse"):
    if (R == None):
        return {"R1": None, "s": 0}

    if(method == "inverse"):
        if (not R.change_ring(RR).is_invertible()):
            return {"R1": None, "s": 0}
        
        try:
            R1 = R.inverse()
            
            R1norms = []
            # compute norms:
            for c in R1.columns():
                R1norms.append(float(c.norm(1)))
            s = floor(1 / (2 * max(R1norms)));
        except:
            return {"R1": None, "s": 0}
    
    elif(method == "gram_schmidt"):
        try:
            R1 = R.transpose().gram_schmidt()[0]
        except:
                return {"R1": None, "s": 0}
        else:
            R1norms = []
            for r in R1.rows():
                R1norms.append(float(r.norm()))
            s = floor(1 / 2 * (min(R1norms)));
    else:
        raise NotImplementedError("Method " + method + " not implemented")
    
    return {"s": s, "R1": R1}

def sigma_lll_statistic():
    tt = 0
    for N in DIMENSIONS:
        r = 0
        for _ in range(COUNT):
            r += 1
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
        
            #generate matrix
            l = 4
            A = random_matrix(ZZ, N, N, x=-l, y=l+1)
            k = floor(sqrt(N) * l)
            B = k * identity_matrix(N)
            R = A + B
            R1 = None
        
            #inverse
            timeInverse = Timer("inverting R")
            timeInverse.start()
            c = compute_s(R)
            invertingTimes.addPoint(N, timeInverse.end())
            sigmaValues.addPoint(N, c["s"])
            
            #LLL
            timeLLL = Timer("inverting R")
            timeLLL.start()
            RLLL = R.transpose().LLL().transpose()
            lllTimings.addPoint(N, timeLLL.end())
                
            #inverse LLL-reduced
            timeInverse = Timer("inverting R")
            timeInverse.start()
            c = compute_s(RLLL)
            invertingTimesR.addPoint(N, timeInverse.end())
            sigmaValuesR.addPoint(N, c["s"])

            save_statistics("s01sigmalll_1_27.txt")
            tt = timeTotal.end()


def str_statistics():
    text = ""
    text += "\n *** Values for sigma W and W/O reduction *** inverse timings  *** \n"
    text += invertingTimes.print_series()
    text += invertingTimesR.print_series()
    text += lllTimings.print_series()
    text += sigmaValues.print_series()
    text += sigmaValuesR.print_series()
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

sigma_lll_statistic()
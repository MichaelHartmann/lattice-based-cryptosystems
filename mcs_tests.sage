PRINT_VARS = False
load('mcs.sage')

COUNT = 10

print("Test if decryption returns the original message:")
N = 10
sigma = 1
time
for _ in range(COUNT):
    S = MCS.GGHParams(N, sigma)
    K = S.keygen()
    m = random_vector(ZZ, N, x=-sigma, y=sigma+1)
    c = S.encrypt(m)
    m1 = S.decrypt(c)

    if(not m == m1):
        print(m)
        print(m1)
        print(c)
        print("----------------------")

print("Test if 0 <= c[i] < B[i,i]:")
for _ in range(COUNT):
    S = MCS.GGHParams(N, sigma)
    K = S.keygen()
    m = random_vector(ZZ, N, x=-sigma, y=sigma)
    c = S.encrypt(m)
    
    B = K["B"]
    BG = (B.transpose().gram_schmidt())[0]

    for i in range(N):
        if(not (0 <= c[i] < B[i,i])):
            print(c)
            print(B)
            print(BG)
            print(BG.inverse()*c)

print("Test attacking:")
for _ in range(COUNT):
    S = MCS.GGHParams(N, sigma)
    K = S.keygen()
    m = random_vector(ZZ, N, x=-sigma, y=sigma)
    c = S.encrypt(m)

    A = MCSAttack(N, K["B"], sigma)
    m1 = A.embeddingAttack(c)

    if(not m == m1):
        print(m)
        print(m1)
        print(c)
        print("----------------------")



load('mcs.sage')

MAXSIGMA = 10

privateKeyTimeSeriesWR = DataSeries("Time for Generating Private Key without Reduction")
publicKeyTimeSeriesWR  = DataSeries("Time for Generating Public Key without Reduction")

privateKeyTimeSeries   = DataSeries("Time for Generating Private Key with LLL Reduction")
publicKeyTimeSeries    = DataSeries("Time for Generating Public Key with LLL Reduction")

encryptionTimeSeries   = DataSeries("Time for Encrypting a Message")
decryptionTimeSeriesRO = DataSeries("Time for Decrypting a Message with RoundOff")
decryptionTimeSeriesNP = DataSeries("Time for Decrypting a Message with Nearest Plane")

successRateSeriesROWR  = [DataSeries("Success Rate of RoundOff without Reduction, s=" + str(i+1)) for i in range(MAXSIGMA)]
successRateSeriesRO    = [DataSeries("Success Rate RoundOff with LLL Reduction, s=" + str(i+1)) for i in range(MAXSIGMA)]
successRateSeriesNPWR  = [DataSeries("Success Rate Nearest Plane without Reduction, s=" + str(i+1)) for i in range(MAXSIGMA)]
successRateSeriesNP    = [DataSeries("Success Rate Nearest Plane with LLL Reduction, s=" + str(i+1)) for i in range(MAXSIGMA)]

def statistics():
    DIMENSIONS = [10*i for i in range(1,10)]
    COUNT = 10

    for N in DIMENSIONS:
        
        timesPrivWR = []
        timesPubWR  = []
        timesPriv   = []
        timesPub    = []
        timesEnc    = []
        timesDec1   = []
        timesDec2   = []
        
        #############################################################
        # Reducing Private Key Disabled                             #
        #############################################################
        
        print("Dimension: " + str(N))
        sys.stdout.flush()
        for _ in range(COUNT):
            M = MCS.Micciancio(N, reducePrivateKey=false)
            K = M.keygen()
            stats = K["statistics"]
            timesPrivWR.append(stats['private'])
            timesPubWR.append(stats['public'])

            for s in range(MAXSIGMA):
                successDec  = 0
                successDec2 = 0
                for _ in range(COUNT):
                    m = random_vector(ZZ, N, x=-s-1, y=s+2)
                    c = M.encrypt(m, timesEnc)
                    m1 = M.decrypt(c, timesDec1)
                    m2 = M.decrypt2(c, timesDec2)

                    if(m == m1):
                        successDec += 1
                    if(m == m2):
                        successDec2 += 1

                successRateSeriesROWR[s].addPoint(N, successDec)
                successRateSeriesNPWR[s].addPoint(N, successDec2)
    
        privateKeyTimeSeriesWR.addPoint(N, round(mean(timesPrivWR)))
        publicKeyTimeSeriesWR.addPoint(N, round(mean(timesPubWR)))
        encryptionTimeSeries.addPoint(N, round(mean(timesEnc)))
        decryptionTimeSeriesRO.addPoint(N, round(mean(timesDec1)))
        decryptionTimeSeriesNP.addPoint(N, round(mean(timesDec2)))
        
        
        #############################################################
        # Reducing Private Key Enabled                              #
        #############################################################
        
        successDec  = 0
        successDec2 = 0
        
        for _ in range(COUNT):
            M = MCS.Micciancio(N, reducePrivateKey=true)
            K = M.keygen()
            stats = K["statistics"]
            timesPriv.append(stats['private'])
            timesPub.append(stats['public'])
            
            for s in range(MAXSIGMA):
                successDec  = 0
                successDec2 = 0
                for _ in range(COUNT):
                    m = random_vector(ZZ, N, x=-s-1, y=s+2)
                    c = M.encrypt(m)
                    m1 = M.decrypt(c)
                    m2 = M.decrypt2(c)
                    
                    if(m == m1):
                        successDec += 1
                    if(m == m2):
                        successDec2 += 1
            
                successRateSeriesRO[s].addPoint(N, successDec)
                successRateSeriesNP[s].addPoint(N, successDec2)

        privateKeyTimeSeries.addPoint(N, round(mean(timesPriv)))
        publicKeyTimeSeries.addPoint(N, round(mean(timesPub)))

        save_statistics()

def str_statistics():
    
    text = ""
    
    text += "\n *** Key Statistics *** \n"
    text += privateKeyTimeSeriesWR.print_series()
    text += publicKeyTimeSeriesWR.print_series()
    text += encryptionTimeSeries.print_series()
    text += decryptionTimeSeriesRO.print_series()
    text += decryptionTimeSeriesNP.print_series()
    text += privateKeyTimeSeries.print_series()
    text += publicKeyTimeSeries.print_series()
    
    text += "\n *** Decoding Statistics *** \n"
    for s in range(MAXSIGMA):
        text += successRateSeriesROWR[s].print_series()
    for s in range(MAXSIGMA):
        text += successRateSeriesNPWR[s].print_series()
    for s in range(MAXSIGMA):
        text += successRateSeriesRO[s].print_series()
    for s in range(MAXSIGMA):
        text += successRateSeriesNP[s].print_series()

    return text

def print_statistics():
    print(str_statistics())

def save_statistics(filename="mcs_statistics.txt"):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

try:
    statistics()
except (KeyboardInterrupt, SystemExit):
    pass
finally:
    print_statistics()
    save_statistics("mcs_statistics_crash.txt")
load('ggh.sage')

N = 200
file = "ggh_challenges/dim" + str(N) + ".sage"
load(file)

BB = matrix(ZZ, N, B)
cc = vector(ZZ, N, c)

A1 = GGHAttack(N, BB, 3)
m1 = A1.embeddingAttack(cc)
print(m1)

"""
A2 = GGHAttack(N, BB.transpose(), 3)
m2 = A2.nguyenAttack(cc)
print(m2)
"""
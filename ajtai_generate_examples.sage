DEBUG = True
PRINT_VARS = False
LATEX = False

RRR = RR.to_prec(16)
load("ajtai.sage")

ld = LatexDefinitions("ajtai_examples.txt", "ajtai")

A = ADCS(4, errorless=false, ld=ld)
K = A.keygen(seed=1)

m = [1,0,1,0,1,0,1,0,1,0]
c = A.encrypt(m, seed=11)
d = A.decrypt(c)

print(m)
print(d)
print(m == d)
ld.define("M", vector(m))
ld.define("C", matrix(c).apply_map(lambda x: round(x,3)))
ld.define("D", vector(d))

print("Errorless: ")

A = ADCS(4, errorless=true, ld=ld)
K = A.keygen(seed=1)

m = [1,0,1,0,1,0,1,0,1,0]
c = A.encrypt(m, seed=11)
d = A.decrypt(c)

print(vector(m) == d)
ld.define("Cerrorless", matrix(c).apply_map(lambda x: round(x,3)))
ld.define("Derrorless", vector(d))

At = ADCSAttack(4, ld=ld)
a = At.nguyenAttack(K[:2])

ld.define("UsApprox", (A.u + a))
ld.close()
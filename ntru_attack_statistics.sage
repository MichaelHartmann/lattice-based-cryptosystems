PRINT_VARS = False
load('ntru.sage')

COUNT = 1
DIM = [25*i for i in range(1, 13)]
SIGMA = 1

privateAttackTimeSeries = DataSeries("Time for Private Key Attack")
messageAttackTimeSeries = DataSeries("Time for Message Attack")

privateAttackTimeSeriesMean = DataSeries("Time for Private Key Attack")
messageAttackTimeSeriesMean = DataSeries("Time for Message Attack")

privateAttackSuccessRate = DataSeries("Success Rate for Private Key Attack")
messageAttackSuccessRate = DataSeries("Success Rate for Message Attack")

def statistics():
    for N in DIM:
        
        D = N.next_prime()
        q = 2**(ceil(log(D,2))+1)
        p = 3
        df = round(0.3*D)
        dg = round(0.15*D)
        dr = round(0.05*D)
        
        print("Dimension: " + str(D))
        sys.stdout.flush()

        successRatePrivate = 0
        successRateMessage = 0
        timePrivate = []
        timeMessage = []
        for _ in range(COUNT):
            
            n = NTRU(D, p, q, df, dg, dr)
            pk = n.keygen()
            m = random_vector(ZZ, D, x=-1, y=2)
            c = n.encrypt(pk, m.list())
    
            print("Key Attack")
            A = NTRUAttack(D, p, q, df, dg, dr)
            (v, B) = A.attackPrivateKey(pk, t=timePrivate)
            privateAttackTimeSeries.addPoint(D, timePrivate[-1])
            
            print("Message Attack")
            (m2, B) = A.attackMessage(pk, c, t=timeMessage)
            messageAttackTimeSeries.addPoint(D, timeMessage[-1])
            if(m2 is not None and vector(m2) == m):
                successRateMessage += 1
        
        privateAttackSuccessRate.addPoint(D, successRatePrivate)
        messageAttackSuccessRate.addPoint(D, successRateMessage)
        
        privateAttackTimeSeriesMean.addPoint(D, round(mean(timePrivate)))
        messageAttackTimeSeriesMean.addPoint(D, round(mean(timeMessage)))

        print("  success-rate: " + str(successRateMessage))
        print("  current mean time: " + str(round(mean(timePrivate)/1000)) + "sec")
        sys.stdout.flush()

def str_statistics():
    text = ""
    text += "\n *** Attack Statistics *** \n"
    text += privateAttackTimeSeries.print_series()
    text += messageAttackTimeSeries.print_series()
    text += privateAttackTimeSeriesMean.print_series()
    text += messageAttackTimeSeriesMean.print_series()
    text += privateAttackSuccessRate.print_series()
    text += messageAttackSuccessRate.print_series()
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()
                                             
def save_statistics(filename="ntru_attack_statistics.txt"):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

try:
    statistics()
except (KeyboardInterrupt, SystemExit):
    pass
finally:
    print_statistics()
    save_statistics("ntru_attack_statistics_crash.txt")
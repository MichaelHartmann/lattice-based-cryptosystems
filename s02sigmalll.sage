################################################################################################################################
# Statistic 02
#
# - How long does it take to compute gram schmidt for finding sigma
#   - Is there a difference if the basis is reduced or not
# - How long does it take to reduce the basis of the private key (also done in Statistic 01)
# - Is there a difference in the quality of the basis (-> larger sigma) if the basis is reduced (wrt. gram schmidt sigma calc)
#
################################################################################################################################

import sys

PRINT_VARS = False

load('definitions.sage')

gsTimes = DataSeries("Time for Gram-Schmidt of non-reduced private key")
gsTimesR = DataSeries("Time for Gram-Schmidt of reduced private key")

lllTimings = DataSeries("Time for reducing private key")

sigmaValues = DataSeries("Value of sigma for non-reduced private key")
sigmaValuesR = DataSeries("Value of sigma for reduced private key")

DIMENSIONS = [i*25 for i in range(18, 29)]
COUNT = 1

def sigma_lll_statistic():
    tt = 0
    for N in DIMENSIONS:
        r = 0
        for _ in range(COUNT):
            r += 1
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
        
            #generate matrix
            l = 4
            A = random_matrix(ZZ, N, N, x=-l, y=l+1)
            k = floor(sqrt(N) * l)
            B = k * identity_matrix(N)
            R = A + B
            R1 = None
        
            #GS
            timeGS = Timer("Gram-Schmidt")
            timeGS.start()
            try:
                RG = R.transpose().gram_schmidt()[0]
            except:
                pass
            else:
                t = timeGS.end()

                gsTimesR.addPoint(N, t)

                # compute norms:
                RGnorms = []
                for i in range(N):
                    RGnorms.append(float(RG.row(i).norm()))
                s = floor(1 / 2 * (min(RGnorms)));

                sigmaValues.addPoint(N, s)
            
            #LLL
            timeLLL = Timer("apply LLL")
            timeLLL.start()
            RLLL = R.transpose().LLL().transpose()
            lllTimings.addPoint(N, timeLLL.end())
                
            #GS
            timeGS = Timer("Gram-Schmidt")
            timeGS.start()
            try:
                RGLLL = RLLL.transpose().gram_schmidt()[0]
            except:
                pass
            else:
                t = timeGS.end()
                
                gsTimes.addPoint(N, t)
                
                # compute norms:
                RGLLLnorms = []
                for i in range(N):
                    RGLLLnorms.append(float(RGLLL.row(i).norm()))
                s = floor(1 / 2 * (min(RGLLLnorms)));
                
                sigmaValuesR.addPoint(N, s)

            save_statistics("s02sigmalll.txt")
            tt = timeTotal.end()

def str_statistics():
    text = ""
    text += "\n *** Values for sigma W and W/O reduction *** inverse timings  *** \n"
    text += gsTimes.print_series()
    text += gsTimesR.print_series()
    text += lllTimings.print_series()
    text += sigmaValues.print_series()
    text += sigmaValuesR.print_series()
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

sigma_lll_statistic()
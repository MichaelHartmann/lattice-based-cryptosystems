def cholesky(G):
    dimensions = G.dimensions()
    assert dimensions[0] == dimensions[1]
    
    n = dimensions[0]
    H = G.change_ring(RR)
    
    for i in range(n):
        for j in range(i+1):
            sum = H[i, j]
            for k in range(j):
                sum = sum - H[i, k] * H[j, k]
            if(i > j):
                H[i, j] = sum / H[j, j]     #Untere Dreiecksmatrix
            elif(sum > 0):                  #Diagonalelement
                H[i, i] = sqrt(sum)         #... ist immer groesser Null
            else:
                exit(1)                     #Die Matrix ist (wenigstens numerisch) nicht symmetrisch positiv definit
    return H
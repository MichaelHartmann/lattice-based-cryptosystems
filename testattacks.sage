load('ggh.sage')

N = 5

T = GGH.GGHTriagonal(N, reducePrivateKey=true);
k = T.keygen()

s = k["s"]
PK = k["B"]

print(s)

m = random_vector(ZZ, N, x=-63, y=64); print("m"); print(m)

c = T.encrypt(m);

A = GGHAttack(N, PK, s);

#m1 = A.embeddingAttack(c); print("m1"); print(m1)

m3 = A.nguyenAttack(c);


#m2 = A.leeAttack(c, m[0:2]); print("m2"); print(m2)
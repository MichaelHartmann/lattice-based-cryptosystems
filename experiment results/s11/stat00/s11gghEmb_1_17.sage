#
# *** s11 GGH attacks  *** 
#
#
# *** Time for Embedding Attack GGH ***
#
#
try:
	S_time_for_embedding_attack_ggh
except:
	S_time_for_embedding_attack_ggh=[]
S_time_for_embedding_attack_ggh+=[[25, 67],[50, 821],[75, 6399],[100, 35497],[125, 64271],[150, 137151],[175, 682197],[200, 1933082],[225, 5817397],[250, 9380502],[275, 17268454],[300, 30892996],[325, 43244588],[350, 68193545],[375, 97747962],[400, 156500174],]#
#
# *** Used Block Size ***
#
#
try:
	S_used_block_size
except:
	S_used_block_size=[]
S_used_block_size+=[[25, 0],[50, 0],[75, 0],[100, 3],[125, 0],[150, 0],[175, 4],[200, 6],[225, 7],[250, 8],[275, 9],[300, 10],[325, 10],[350, 11],[375, 12],[400, 13],]#
#
# *** Success Rate ***
#
#
try:
	S_success_rate
except:
	S_success_rate=[]
S_success_rate+=[[25, 1],[50, 1],[75, 1],[100, 0],[125, 1],[150, 1],[175, 1],[200, 0],[225, 0],[250, 0],[275, 0],[300, 0],[325, 0],[350, 0],[375, 0],[400, 0],]
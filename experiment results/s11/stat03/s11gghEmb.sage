################################################################################################################################
# Statistic 11
#
# - How long does it take to attack MCS with Embedding Attack
# - What are the block sizes to successfully attack it
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ggh.sage')

attackTimes = DataSeries("Time for Embedding Attack GGH")
blockSize = DataSeries("Used Block Size")
success = DataSeries("Success Rate")


def ggh_emb_attack_statistics(x=1, y=33, ocount=10, count=10):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT = count
    
    for _ in range(ocount):
        tt = 0
        for N in DIMENSIONS:
            r = 0
            for _ in range(COUNT):
                r += 1
                timeTotal = Timer("Total")
                timeTotal.start()
                sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
                sys.stdout.flush()
                    
                M = GGH.GGH1996(N)
                k = M.keygen()
                pk = k["B"]
                
                A = GGHAttack(N, pk, 1)
                

                m = random_vector(ZZ, N, x=-128, y=128)
                c = M.encrypt(m)
                
                t = []
                n = []
                m2 = A.embeddingAttack(c, t=t, neededBlockSize=n)
                attackTimes.addPoint(N, t[-1])
                blockSize.addPoint(N, n[-1])
                
                if(m2 == m):
                    success.addPoint(N, 1)
                else:
                    success.addPoint(N, 0)
                
                save_statistics("s11gghEmb_" + str(x) + "_" + str(y) + ".sage")
                tt = timeTotal.end()

def str_statistics():
    text = "#"
    text += "\n# *** s11 GGH attacks  *** \n"
    text += attackTimes.print_series()
    text += blockSize.print_series()
    text += success.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ggh_emb_attack_statistics(x, y, outer count, inner count), where x is the start value (times 25), and y-1 is the end value (times 25)")

################################################################################################################################
# Statistic 16
#
# - GGH decrypting with too high sigma
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ggh.sage')
success1 = DataSeries("success sigma 1")
success2 = DataSeries("success sigma 2")
success4 = DataSeries("success sigma 4")
success8 = DataSeries("success sigma 8")
success12 = DataSeries("success sigma 12")

def ggh_statistics(x=1, y=33, c1=1, c=10):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT1 = c1
    COUNT = c
    
    tt = 0
    for N in DIMENSIONS:
        r = 0
        
        suc1 = 0
        suc2 = 0
        suc4 = 0
        suc8 = 0
        suc12 = 0

        for _ in range(COUNT1):
            timeTotal = Timer("Total")
            timeTotal.start()
            r += 1
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
                
            G = GGH.GGH1996(N)
            k = G.keygen(false)

            suc1 = suc2 = suc4 = suc8 = suc12 = 0
            
            for _ in range(COUNT):
            
                m1 = random_vector(ZZ, N, x=-1, y=2)
                m2 = random_vector(ZZ, N, x=-2, y=3)
                m4 = random_vector(ZZ, N, x=-4, y=5)
                m8 = random_vector(ZZ, N, x=-8, y=9)
                m12 = random_vector(ZZ, N, x=-12, y=13)

                c1 = G.encrypt(m1, 1)
                c2 = G.encrypt(m2, 2)
                c4 = G.encrypt(m4, 4)
                c8 = G.encrypt(m8, 8)
                c12 = G.encrypt(m12, 12)
                
                d1 = G.decrypt(c1)
                d2 = G.decrypt(c2)
                d4 = G.decrypt(c4)
                d8 = G.decrypt(c8)
                d12 = G.decrypt(c12)
                
                if (d1 == m1):
                    suc1 += 1
                if (d2 == m2):
                    suc2 += 1
                if (d4 == m4):
                    suc4 += 1
                if (d8 == m8):
                    suc8 += 1
                if (d12 == m12):
                    suc12 += 1

            tt = timeTotal.end()
    
            success1.addPoint(N, suc1/COUNT)
            success2.addPoint(N, suc2/COUNT)
            success4.addPoint(N, suc4/COUNT)
            success8.addPoint(N, suc8/COUNT)
            success12.addPoint(N, suc12/COUNT)
        
            save_statistics("s16gghTooHighSigma_" + str(x) + "_" + str(y) + ".sage")

def str_statistics():
    text = "\n#"
    text += "\n# *** s16 GGH too high sigma  *** \n"
    
    text += success1.print_series()
    text += success2.print_series()
    text += success4.print_series()
    text += success8.print_series()
    text += success12.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ggh_statistics(x, y, c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")
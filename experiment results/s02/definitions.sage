import time
import random
import sys

#lambdas:
current_milli_time = lambda: int(round(time.time() * 1000))

numb = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"]

def printv(v, force=false):
    if(PRINT_VARS | force):
        print(v)
        sys.stdout.flush()

class Timer:
    
    def __init__(self, name):
        self.name = name
    
    def start(self):
        self.start = current_milli_time()
    
    def end(self):
        self.end = current_milli_time()
        time = self.printTime()
        return time
    
    def printTime(self):
        if(self.start is not None):
            self.diff = self.end - self.start;
            printv("Time for " + self.name + ": " + str(self.diff) + "ms")
            return self.diff
        else:
            print("error in time calculation (start=" + str(self.start) + ", end=" + str(self.end) +")")
            return -1
    
    def getTime(self):
        return self.diff

class Log:
    
    file = None
    
    def __init__(self, fileName, isDebug = True):
        self.file = open(fileName,"w")
        self.isDebug = isDebug
    
    def close(self):
        print("close")
        self.file.close()
    
    def log(self, varName, value, l=True):
        if(self.isDebug):
            print(varName, str(value))
            if (l and self.file):
                self.file.write(time.strftime(" %H:%M:%S:\n") + str(varName) + " = "  + latex(value)  + "$\\\\ \n")

class LatexDefinitions:
    
    file = None
    
    def __init__(self, fileName, prefix):
        self.file = open(fileName,"w")
        self.prefix = prefix
    
    def close(self):
        self.file.close()
    
    def define(self, d, v):
        self.file.write("\def \\" + self.prefix + d + "{" + latex(v) + "} \n");

class NoneDefiner:
    def close(self):
        pass
    def define(self, d, v):
        pass

class DataSeries:
    __name = "default"
    __series = None
    def __init__(self, name):
        self.__name = name
        self.__series = []
    def addPoint(self, x, y):
        self.__series.append((x,y))
    def print_series(self):
        s = "#\n#\n# *** " + self.__name + " ***\n#\n#"
        s += "\ntry:\n\tS_" + self.__name.lower().replace(" ", "_") +"\nexcept:\n"
        s += "\tS_" + self.__name.lower().replace(" ", "_") + "=[]\n"
        s += "S_" + self.__name.lower().replace(" ", "_") +"+=["
        for k in self.__series:
            s += "[" + str(k[0]) + ", " + str(k[1]) + "],"
        s += "]"
        return s

def bundle_current_dir():
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if(f[0] == "."):
            continue
        load(f)

def list_apply(S, fkt):
    l = []
    for s in S:
        l.append(fkt(s))
    return l

def divide1000(e):
    return (e[0], e[1]/1000)

def list2tikz(S):
    s = ""
    s += "\\addplot[dashed, only marks, color=black]\n"
    s += "plot coordinates {\n"
    for e in S:
        s += "(" + str(e[0]) + ", " + str(e[1]) + ")\n"
    s += "};\n"
    print(s)

def heightB(B):
    min_norm = infinity
    BG = B.transpose().gram_schmidt()[0]
    for col in BG.columns():
        n = abs(col.norm())
        if(n < min_norm):
            min_norm = n
    return n

def odef(B):
    det = B.determinant()
    BG = B.transpose().gram_schmidt()[0]
    p = 1
    for col in BG.columns():
        p *= abs(col.norm())
    return 1/det * p

def plotSeries(series):
    var('d,e,f,g,h')
    m(x) = d*x^4 + e*x^3 + f*x^2 + g*x +h
    ff = find_fit(series,m,solution_dict=True)
    print(ff)
    print(m(d=ff[d],e=ff[e],f=ff[f],g=ff[g],h=ff[h])(x))
    return points(series,color='purple') + plot(m(d=ff[d],e=ff[e],f=ff[f],g=ff[g],h=ff[h],),(x,0,series[-1][0]*1.5),color='red')


import gzip
import os
# Return the size (in bytes) of the compressed object using gzip
def getSize(obj):
    
    rand = random.randint(1000000000, 9999999999)
    fileName = "tmp_" + str(rand) + ".txt.gz"
    
    f = gzip.open(fileName, 'wb', compresslevel=9)
    f.write(obj)
    f.close()

    size = os.path.getsize(fileName)
    
    os.remove(fileName)
    
    return size
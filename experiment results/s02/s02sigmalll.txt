
 *** Values for sigma W and W/O reduction *** inverse timings  *** 
#
#
# *** Time for Gram-Schmidt of non-reduced private key ***
#
#
try:
	S_time_for_gram-schmidt_of_non-reduced_private_key
except:
	S_time_for_gram-schmidt_of_non-reduced_private_key=[]
S_time_for_gram-schmidt_of_non-reduced_private_key+=[[450, 8272480],[475, 10453033],[500, 12600202],[525, 15777021],[550, 19115369],[575, 22741815],[600, 27403911],[625, 32790436],[650, 42289587],[675, 49179788],[700, 58248229],]#
#
# *** Time for Gram-Schmidt of reduced private key ***
#
#
try:
	S_time_for_gram-schmidt_of_reduced_private_key
except:
	S_time_for_gram-schmidt_of_reduced_private_key=[]
S_time_for_gram-schmidt_of_reduced_private_key+=[[450, 8311174],[475, 10639560],[500, 12904390],[525, 15585655],[550, 19197439],[575, 22855348],[600, 27481581],[625, 33072472],[650, 39429093],[675, 47058021],[700, 59628729],]#
#
# *** Time for reducing private key ***
#
#
try:
	S_time_for_reducing_private_key
except:
	S_time_for_reducing_private_key=[]
S_time_for_reducing_private_key+=[[450, 28259],[475, 34354],[500, 39440],[525, 44129],[550, 51100],[575, 61715],[600, 67077],[625, 85040],[650, 93437],[675, 119804],[700, 133219],]#
#
# *** Value of sigma for non-reduced private key ***
#
#
try:
	S_value_of_sigma_for_non-reduced_private_key
except:
	S_value_of_sigma_for_non-reduced_private_key=[]
S_value_of_sigma_for_non-reduced_private_key+=[[450, 31],[475, 31],[500, 31],[525, 32],[550, 33],[575, 33],[600, 33],[625, 36],[650, 34],[675, 36],[700, 38],]#
#
# *** Value of sigma for reduced private key ***
#
#
try:
	S_value_of_sigma_for_reduced_private_key
except:
	S_value_of_sigma_for_reduced_private_key=[]
S_value_of_sigma_for_reduced_private_key+=[[450, 34],[475, 36],[500, 37],[525, 37],[550, 38],[575, 39],[600, 39],[625, 41],[650, 41],[675, 42],[700, 42],]
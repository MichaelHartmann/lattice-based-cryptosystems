import sys

DEBUG = True
PRINT_VARS = False
LATEX = False

load('definitions.sage')

################################
# NTRU-specific helpers        #
################################

#Takes d1 (and optionally d2) and returns a list of coefficients with d1 1's and d2 (-1)'s and the rest 0 when random_element is called
class Lset:
    def __init__(self, N, d1, d2=None):
        self.__N = N
        self.__d1 = d1
        if (d2 is None):
            self.__d2 = d1
        else:
            self.__d2 = d2

    def random_element(self):
        t1 = sample(set(range(self.__N)), self.__d1)
        t2 = sample(set(range(self.__N)) - set(t1), self.__d2)
        res = [0 for _ in range(self.__N)]
        for i in t1:
            res[i] = 1;
        for i in t2:
            res[i] = -1;
        return res

def width(p):
    return max(p.list()) - min(p.list())

def increase_min_coeff(p, modulus):
    coeff = p.list()
    min_coeff = min(coeff)
    for i in range(len(coeff)):
        if(coeff[i] == min_coeff):
            coeff[i] += modulus
    return coeff

def find_m1(B, q, p, h, r1, e):
    Zq = Integers(q)
    lower_bound = B - ceil((q-1)/2)
    upper_bound = B + floor(q/2)
    qon = Zq(e(1)-p*r1*h(1));
    
    m1 = lower_bound - 1;
    while(m1 < upper_bound):
        m1 += 1
        if(qon == m1 % q):
            break
    return m1

#make the circulant matrix of a vector given as parameter
def circulant_matrix(v_orig):
    #helper function: move the vector by 1 position
    def move_col(v):
        r = [v.pop()]
        r.extend(v)
        return r
        
    v = list(v_orig)
    l = len(v);
    m = matrix(l);
    for i in range(l):
        m[:,i] = vector(v).column()
        v = move_col(v)
    return m

def check_a(a,f,g,r1,m1,p,q):
    #Property 1:
    p1 = a(1) == f(1)*m1+p*r1*g(1)
    if(not p1):
        print("p1 failed")
    #Property 2:
    p2 = width(a) < q
    if(not p2):
        print("p2 failed")
    return p1 and p2

def check_decryption_succeeded(a, q, d, exp_m1):
    p1 = width(a) < q
    p2 = d(1) == exp_m1

    return p1 and p2

def inv_mod(a,p,r,b,R):
    q = p
    while(q < p**r):
        q = q**2
        Rq, _ = PolynomialRing(Integers(q), "V").objgen()
        b = R(Rq(b*(2-a*b)).list())
    return b

def check_ones(ones):
    
    def function(B):
        for b in B.rows():
            bl = b.list()
            if(bl.count(1) == ones):
                return b
        return None

    return function

def adjust_list(N, l):
    v = vector(ZZ, N)
    for i in range(len(list(l))):
        v[i] = l[i]
    return v.list()



BR = None

#reduces given Basis B with LLL, then with BKZ and increasing blocksize and checks after every step if checkFkt is not None. If so, the reduction stops and returns the result of checkFkt and the reduced basis
def reduce(B, checkFkt, neededBlockSize=[]):
    global BR
    
    #print("*** try with LLL: ***")
    B = B.LLL()
    BR = B
    bs = 0
    
    result = checkFkt(B)
    if(result is not None):
        neededBlockSize.append(bs)
        return (result, B)

    bs = 2
    while(True):
        bs += 1
        #print("*** try with BKZ (block_size=%(bs)s): ***" % locals())
        neededBlockSize.append(bs)
        try:
            B = B.BKZ(block_size=bs)
        except RuntimeError as r:
            #print(" *** ERROR while BKZ reduction *** ")
            print(r.message)

            return (None, B)
        BR = B
        result = checkFkt(B)
        if(result is not None):
            return (result, B)
        if(bs > 25):
            return (None, B)

################################
# NTRU-System                  #
################################

class NTRU:
    
    def __init__(self, N, p, q, df, dg, dr):

        #parameter tests:
        if (gcd(p,q) != 1):
            print("p and q must be relatively prime")
            sys.exit(0)
        if (df > N/2):
            print("df too big")
            sys.exit(0)
        if (dg > N/2):
            print("dg too big")
            sys.exit(0)
        if (dr > N/2):
            print("dr too big")
            sys.exit(0)
        if (df < 1):
            print("df too small")
            sys.exit(0)

        self.N = N
        self.p = p
        self.q = q

        self.__repr = "NTRU instance with with N=%(N)s, p=%(p)s, q=%(q)s, df=%(df)s, dg=%(dg)s, dr=%(dr)s" % locals()
        
        R, X = PolynomialRing(ZZ, "X").objgen()
        self.R = R
        self.I = self.R(X^self.N-1)

        Rp.<Y> = GF(self.p, "y")[]
        self.Rp = Rp
        self.Ip = self.Rp(self.I)
        Rq.<Z> = GF(self.q, "Z")[]
        self.Rq = Rq
        self.Iq = self.Rq(self.I)
        R2.<W> = GF(2, "W")[]
        self.R2 = R2
        self.I2 = self.R2(self.I)

        self.df = df
        self.dg = dg
        self.dr = dr

        self.Lf = Lset(N, df, df-1)
        self.Lg = Lset(N, dg, dg)
        self.Lr = Lset(N, dr, dr) #change also r1 when change definition of Lr
        
        #keys
        self.f   = None
        self.fp  = None
        self.fq  = None
        self.g   = None
        self.h   = None

    def __repr__(self):
        return self.__repr
    
    def getPrivateKey(self):
        return (self.f, self.g, self.fp, self.fq)
    
    def keygen(self, f=None, g=None, t1=[], t2=[]):
        timerKeyGen = Timer("Key Generation");
        timerKeyGen.start()

        timerPrivate = Timer("generate Private Key")
        timerPrivate.start()
        MAX_COUNT = 10
        if (f is not None and g is not None):
            MAX_COUNT = 1

        count = 0
        while(count < MAX_COUNT):
            count += 1
            if(f is None):
                self.f = self.R(self.Lf.random_element())
            else:
                self.f = self.R(f)
            if(g is None):
                self.g = self.R(self.Lg.random_element())
            else:
                self.g = self.R(g)

            try:
                timerFp = Timer(" - computing fp", "mu")
                timerFp.start()
                fp  = self.Rp(self.f).inverse_mod(self.Ip)
                timerFp.end()
                
                timerFq = Timer(" - computing fq", "mu")
                timerFq.start()
                f21 = self.R2(self.f).inverse_mod(self.I2)
                i   = inv_mod(self.f, 2, log(self.q,2), self.R(f21), self.R) % self.I
                fq  = self.R((vector(i.list()) % self.q).list())
            except ValueError:
                pass
            else:
                break
    
        if (self.Rp(self.f * self.R(fp) % self.I) != 1):
            print("FALSE")

        timerFq.end()
        t1.append(timerPrivate.end())
        

        self.fp = fp
        self.fq = fq

        timerPublic = Timer("generating Public Key", "mu")
        timerPublic.start()
        self.h = self.R((vector((self.p*self.R(self.fq)*self.g % self.I).list()) % self.q).list())
        t2.append(timerPublic.end())
        
        timerKeyGen.end()
        
        return self.h

    def encrypt(self, publicKey, message, r=None):
        m = self.R(message)
        
        if(r is None):
            r = self.R(self.Lr.random_element())
        else:
            r = self.R(r)

        self.r = r
        timerE = Timer(" - computing e")
        timerE.start()
        e = (r*publicKey + m) % self.I
        timerE.end()
        self.e = e
        return e

    def __align_interval(self,l):
        v = [Integer(l[i] % self.q) for i in range(self.N)]
        for i in range(self.N):
            if (v[i] < -self.q/2):
                v[i] = self.q + v[i]
            elif(v[i] >= self.q/2):
                v[i] = v[i] - self.q
        return v

    def decrypt(self, e):
        a = self.f*e % self.I
        aq = self.__align_interval(a)
        ap = (vector(aq) % self.p).list()
        
        self.a = a
        self.aq = aq
        self.ap = ap

        d = self.fp * self.Rp(ap) % self.Ip
        return d.list()


################################
# Attack                       #
################################

class NTRUAttack:
    
    def __init__(self, N, p, q, df, dg, dr):
        self.N = N
        self.p = p
        self.q = q
        self.df = df
        self.dg = dg
        self.dr = dr
        self.ZZp = Integers(p)
        self.ZZq = Integers(q)
    
    #NTRU lattice
    def ntru_lattice(self, H):
        m = identity_matrix(ZZ, 2*self.N)
        for i in range(self.N, 2*self.N):
            m[i,i] = self.q
        m[self.N:,:self.N] = H
        return m
    
    def attackPrivateKey(self, pk, t=[], neededBlockSize=[]):

        h = adjust_list(self.N, pk)
        self.publicKeyMatrix = circulant_matrix(h)

        timerAtt = Timer("attacking private key"); timerAtt.start()

        Pq = Integer(self.ZZq(self.p)**(-1));

        l = self.ntru_lattice(Pq*self.publicKeyMatrix).transpose()
        
        (v, B) = reduce(l, check_ones(self.df+self.dg), neededBlockSize)

        t.append(timerAtt.end())
        self.l = l
        
        return (v, B)

    def attackMessage(self, pk, e, t=[], neededBlockSize=[]):
        
        h = adjust_list(self.N, pk)
        self.publicKeyMatrix = circulant_matrix(h)
        
        e = adjust_list(self.N, e)
        ee = vector(vector(ZZ, self.N).list() + list(e) + [1])
        
        timerAtt = Timer("attacking message"); timerAtt.start()
        
        l = self.ntru_lattice(self.publicKeyMatrix).transpose()
        l = l.augment(vector(ZZ, 2*self.N))
        l = l.stack(ee)
        
        self.l = l
        
        def chk(B):
            try:
                pos = B.column(-1).list().index(1)
            except:
                try:
                    pos = B.column(-1).list().index(-1)
                except:
                    return None
            r = B.row(pos)
            for e in r[:-1]:
                if(not e in range(-1,2)):
                    return None
            return r[self.N:-1]
        
        (v, B) = reduce(l, chk, neededBlockSize)
        
        t.append(timerAtt.end())

        return (v, B)

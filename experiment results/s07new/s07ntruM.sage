################################################################################################################################
# Statistic 07
#
# - Timings of NTRU attack (on message)
# - Used block size
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ntru.sage')

messageAttackTimeSeries = DataSeries("Time for Message Attack")
messageAttackNeededBlockSize = DataSeries("Needed Block Size")
messageAttackNeededSuccessRate = DataSeries("SucessRate")


def ntru_m_attack_statistic(x=1, y=33, c1=1, c=10):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT1 = c1
    COUNT = c
    
    tt = 0
    for N in DIMENSIONS:
        
        D = N.next_prime()
        q = 2**(ceil(log(D,2))+1)
        p = 3
        df = round(0.3*D)
        dg = round(0.15*D)
        dr = round(0.05*D)

        r = 0
        for _ in range(COUNT1):
            timeTotal = Timer("Total")
            timeTotal.start()
            r += 1
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
                
            timeMessage = []
            neededBlockSize = []

            n = NTRU(D, p, q, df, dg, dr)
            pk = n.keygen()
            m = random_vector(ZZ, D, x=-1, y=2)
            c = n.encrypt(pk, m.list())
            
            A = NTRUAttack(D, p, q, df, dg, dr)
            (m2, B) = A.attackMessage(pk, c, t=timeMessage, neededBlockSize=neededBlockSize)
            messageAttackTimeSeries.addPoint(D, timeMessage[-1])
            messageAttackNeededBlockSize.addPoint(D, neededBlockSize[-1])

            successRateMessage = 0
            if(m2 is not None and vector(m2) == m):
                successRateMessage += 1

            messageAttackNeededSuccessRate.addPoint(D, successRateMessage)
    
            save_statistics("s07ntruM_" + str(x) + "_" + str(y) + ".sage")

        tt = timeTotal.end()

def str_statistics():
    text = "\n#"
    text += "\n# *** s07 Attack Statistics  *** \n"
    text += messageAttackTimeSeries.print_series()
    text += messageAttackNeededBlockSize.print_series()
    text += messageAttackNeededSuccessRate.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ntru_m_attack_statistic(x, y, c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")

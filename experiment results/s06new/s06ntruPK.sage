################################################################################################################################
# Statistic 06
#
# - Timings of NTRU attack (on private key)
# - Used block size
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ntru.sage')

privateAttackTimeSeries = DataSeries("Time for Private Key Attack")
privateAttackNeededBlockSize = DataSeries("Needed Block Size")


def ntru_pk_attack_statistic(x=1, y=33, c1=1, c=10):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT1 = c1
    COUNT = c
    
    tt = 0
    for N in DIMENSIONS:
        
        D = N.next_prime()
        q = 2**(ceil(log(D,2))+1)
        p = 3
        df = round(0.3*D)
        dg = round(0.15*D)
        dr = round(0.05*D)

        r = 0
        for _ in range(COUNT1):
            timeTotal = Timer("Total")
            timeTotal.start()
            r += 1
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
                
            timePrivate = []
            neededBlockSize = []

            n = NTRU(D, p, q, df, dg, dr)
            pk = n.keygen()
            
            A = NTRUAttack(D, p, q, df, dg, dr)
            (v, B) = A.attackPrivateKey(pk, t=timePrivate, neededBlockSize=neededBlockSize)
            privateAttackTimeSeries.addPoint(D, timePrivate[-1])
            privateAttackNeededBlockSize.addPoint(D, neededBlockSize[-1])
    
            save_statistics("s06ntruPK_" + str(x) + "_" + str(y) + ".sage")

        tt = timeTotal.end()

def str_statistics():
    text = "\n#"
    text += "\n# *** s06 Attack Statistics  *** \n"
    text += privateAttackTimeSeries.print_series()
    text += privateAttackNeededBlockSize.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ntru_pk_attack_statistic(x, y, c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")

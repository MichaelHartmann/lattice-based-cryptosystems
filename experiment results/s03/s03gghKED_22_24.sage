
#
# *** s03 GGH timings  *** 
#
#
# *** Generate Private Key ***
#
#
try:
	S_generate_private_key
except:
	S_generate_private_key=[]
S_generate_private_key+=[[525, 10939195],[550, 11529562],[575, 14754382],]#
#
# *** Generate Public Key ***
#
#
try:
	S_generate_public_key
except:
	S_generate_public_key=[]
S_generate_public_key+=[[525, 45000],[550, 52108],[575, 57764],]#
#
# *** Encrypt ***
#
#
try:
	S_encrypt
except:
	S_encrypt=[]
S_encrypt+=[[525, 128],[525, 46],[525, 46],[525, 45],[525, 48],[525, 47],[525, 81],[525, 46],[525, 46],[525, 47],[550, 222],[550, 84],[550, 89],[550, 92],[550, 84],[550, 90],[550, 51],[550, 50],[550, 50],[550, 52],[575, 246],[575, 93],[575, 60],[575, 58],[575, 57],[575, 55],[575, 89],[575, 60],[575, 60],[575, 57],]#
#
# *** Decrypt with RoundOff ***
#
#
try:
	S_decrypt_with_roundoff
except:
	S_decrypt_with_roundoff=[]
S_decrypt_with_roundoff+=[[525, 1737],[525, 1689],[525, 2131],[525, 1716],[525, 1674],[525, 2892],[525, 1691],[525, 1685],[525, 1676],[525, 1685],[550, 3257],[550, 3143],[550, 3133],[550, 3141],[550, 3142],[550, 1878],[550, 1869],[550, 1869],[550, 1867],[550, 1903],[575, 3404],[575, 1947],[575, 2005],[575, 1993],[575, 2001],[575, 3281],[575, 1967],[575, 2045],[575, 1960],[575, 2009],]#
#
# *** success sigma 1 ***
#
#
try:
	S_success_sigma_1
except:
	S_success_sigma_1=[]
S_success_sigma_1+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 2 ***
#
#
try:
	S_success_sigma_2
except:
	S_success_sigma_2=[]
S_success_sigma_2+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 4 ***
#
#
try:
	S_success_sigma_4
except:
	S_success_sigma_4=[]
S_success_sigma_4+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 8 ***
#
#
try:
	S_success_sigma_8
except:
	S_success_sigma_8=[]
S_success_sigma_8+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 16 ***
#
#
try:
	S_success_sigma_16
except:
	S_success_sigma_16=[]
S_success_sigma_16+=[[525, 0],[550, 0],[575, 0],]#
#
# *** success sigma 1 (with reduced key) ***
#
#
try:
	S_success_sigma_1__with_reduced_key
except:
	S_success_sigma_1__with_reduced_key=[]
S_success_sigma_1__with_reduced_key+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 2 (with reduced key) ***
#
#
try:
	S_success_sigma_2__with_reduced_key
except:
	S_success_sigma_2__with_reduced_key=[]
S_success_sigma_2__with_reduced_key+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 4 (with reduced key) ***
#
#
try:
	S_success_sigma_4__with_reduced_key
except:
	S_success_sigma_4__with_reduced_key=[]
S_success_sigma_4__with_reduced_key+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 8 (with reduced key) ***
#
#
try:
	S_success_sigma_8__with_reduced_key
except:
	S_success_sigma_8__with_reduced_key=[]
S_success_sigma_8__with_reduced_key+=[[525, 10],[550, 10],[575, 10],]#
#
# *** success sigma 16 (with reduced key) ***
#
#
try:
	S_success_sigma_16__with_reduced_key
except:
	S_success_sigma_16__with_reduced_key=[]
S_success_sigma_16__with_reduced_key+=[[525, 0],[550, 0],[575, 0],]
#
# *** s03 GGH sizes  *** 
#
#
# *** Size Private Key ***
#
#
try:
	S_size_private_key
except:
	S_size_private_key=[]
S_size_private_key+=[[525, 153871],[550, 168508],[575, 184018],]#
#
# *** Size Public Key ***
#
#
try:
	S_size_public_key
except:
	S_size_public_key=[]
S_size_public_key+=[[525, 453272],[550, 504829],[575, 528468],]#
#
# *** Size Ciphertext ***
#
#
try:
	S_size_ciphertext
except:
	S_size_ciphertext=[]
S_size_ciphertext+=[[525, 41],[550, 41],[575, 41],]
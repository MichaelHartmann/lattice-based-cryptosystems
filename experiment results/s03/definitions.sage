import time
import random
import sys

#lambdas:
current_mu_time = lambda: int(round(time.time() * 1000000))

numb = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"]

def printv(v, force=false):
    if(PRINT_VARS | force):
        print(v)
        sys.stdout.flush()

class Timer:
    
    def __init__(self, name, unit="ms"):
        self.name = name
        self.unit = unit
        if (not unit in ["ms", "mu", "s"]):
            raise NotImplementedError("only mu, ms and s implemented")
    
    def start(self):
        self.start = current_mu_time()
    
    def end(self):
        self.end = current_mu_time()
        time = self.printTime()
        return time
    
    def printTime(self):
        if(self.start is not None):
            self.diff = self.end - self.start;
            printv("Time for " + self.name + ": " + str(self.getTime()) + self.unit)
            return self.getTime()
        else:
            print("error in time calculation (start=" + str(self.start) + ", end=" + str(self.end) +")")
            return -1
    
    def getTime(self):
        r = self.diff
        if (self.unit == "ms"):
            r = round(r/1000)
        elif (self.unit == "s"):
            r = round(r/1000000)
        return r

class Log:
    
    file = None
    
    def __init__(self, fileName, isDebug = True):
        self.file = open(fileName,"w")
        self.isDebug = isDebug
    
    def close(self):
        print("close")
        self.file.close()
    
    def log(self, varName, value, l=True):
        if(self.isDebug):
            print(varName, str(value))
            if (l and self.file):
                self.file.write(time.strftime(" %H:%M:%S:\n") + str(varName) + " = "  + latex(value)  + "$\\\\ \n")

class LatexDefinitions:
    
    file = None
    
    def __init__(self, fileName, prefix):
        self.file = open(fileName,"w")
        self.prefix = prefix
    
    def close(self):
        self.file.close()
    
    def define(self, d, v):
        self.file.write("\def \\" + self.prefix + d + "{" + latex(v) + "} \n");

class NoneDefiner:
    def close(self):
        pass
    def define(self, d, v):
        pass

class DataSeries:
    __name = "default"
    __series = None
    def __init__(self, name):
        self.__name = name
        self.__series = []
    def addPoint(self, x, y):
        self.__series.append((x,y))
    def print_series(self):
        s = "#\n#\n# *** " + self.__name + " ***\n#\n#"
        s += "\ntry:\n\tS_" + self.__name.lower().replace(" ", "_").replace("(", "_").replace(")", "") +"\nexcept:\n"
        s += "\tS_" + self.__name.lower().replace(" ", "_").replace("(", "_").replace(")", "") + "=[]\n"
        s += "S_" + self.__name.lower().replace(" ", "_").replace("(", "_").replace(")", "") +"+=["
        for k in self.__series:
            s += "[" + str(k[0]) + ", " + str(k[1]) + "],"
        s += "]"
        return s

def bundle_current_dir():
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if(f[0] == "."):
            continue
        load(f)

def list_apply(S, fkt):
    l = []
    for s in S:
        l.append(fkt(s))
    return l

def divide1000(e):
    return (e[0], e[1]/1000)

def uniqueList(S):
    S1 = []
    for e in S:
        S1.append(tuple(e))
    return uniq(S1)

def list2tikz(S):
    s = ""
    s += "\\addplot[dashed, only marks, color=black]\n"
    s += "plot coordinates {\n"
    for e in S:
        s += "(" + str(e[0]) + ", " + str(e[1]) + ")\n"
    s += "};\n"
    print(s)

def heightB(B):
    min_norm = infinity
    BG = B.transpose().gram_schmidt()[0]
    for col in BG.columns():
        n = abs(col.norm())
        if(n < min_norm):
            min_norm = n
    return n

def odef(B):
    det = B.determinant()
    BG = B.transpose().gram_schmidt()[0]
    p = 1
    for col in BG.columns():
        p *= abs(col.norm())
    return 1/det * p

def plotSeries(series):
    var('d,e,f,g,h')
    m4(x) = d*x^4 + e*x^3 + f*x^2 + g*x +h
    ff4 = find_fit(series,m4,solution_dict=True)
    m3(x) = e*x^3 + f*x^2 + g*x +h
    ff3 = find_fit(series,m3,solution_dict=True)
    m2(x) = f*x^2 + g*x +h
    ff2 = find_fit(series,m2,solution_dict=True)
    m1(x) = f*x*log(g*x,2)+h
    ff1 = find_fit(series,m1,solution_dict=True)
    m0(x) = g*x+h
    ff0 = find_fit(series,m0,solution_dict=True)

    print("red:")
    print(m4(d=ff4[d],e=ff4[e],f=ff4[f],g=ff4[g],h=ff4[h])(x))
    print("orange:")
    print(m3(e=ff3[e],f=ff3[f],g=ff3[g],h=ff3[h])(x))
    print("yellow:")
    print(m2(f=ff2[f],g=ff2[g],h=ff2[h])(x))
    print("green:")
    print(m1(f=ff1[f], g=ff1[g],h=ff1[h])(x))
    print("blue:")
    print(m0(g=ff0[g],h=ff0[h])(x))

    return points(series,color='purple') + plot(m4(d=ff4[d],e=ff4[e],f=ff4[f],g=ff4[g],h=ff4[h],),(x,0,series[-1][0]*1.5),color='red') + plot(m3(e=ff3[e],f=ff3[f],g=ff3[g],h=ff3[h],),(x,0,series[-1][0]*1.5),color='orange') + plot(m2(f=ff2[f],g=ff2[g],h=ff2[h],),(x,0,series[-1][0]*1.5),color='yellow') + plot(m1(f=ff1[f], g=ff1[g],h=ff1[h],),(x,0,series[-1][0]*1.5),color='green') + plot(m0(g=ff0[g],h=ff0[h],),(x,0,series[-1][0]*1.5),color='blue')


import gzip
import os
# Return the size (in bytes) of the compressed object using gzip
def getSize(obj):
    
    rand = random.randint(1000000000, 9999999999)
    fileName = "tmp_" + str(rand) + ".txt.gz"
    
    f = gzip.open(fileName, 'wb', compresslevel=9)
    f.write(obj)
    f.close()

    size = os.path.getsize(fileName)
    
    os.remove(fileName)
    
    return size
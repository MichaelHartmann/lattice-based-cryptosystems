DEBUG = True
PRINT_VARS = False
LATEX = False

load('definitions.sage')

################################
# GGH-specific helpers         #
################################

def count_zeros(v):
    sum = 0
    for i in range(len(v)):
        if (v[i] == 0):
            sum = sum + 1
    return sum

def check_solution_found(B, domain):
    for b in B.columns():
        for e in b[0:-1]:
            if(e not in domain):
                break               #jump to outer loop
        else:                       #for...else exists in python. Else block is executed if for does not break
            return b
    return None

def CVP2SVP(B, t, algorithm="LLL", delta=None, block_size=10, prune=0, domain=None, bs_stat=[]):
    tt = list(t)
    tt.append(1)
    if(domain is not None):
        print("domain:" + str(domain))
        try:
            
            M = matrix(ZZ, B.nrows() + 1, B.ncols() + 1)
            M[:, 0] = vector(tt)
            M[:-1, 1:] = B
            #LLL and check for solution
            print("reduce with LLL:")
            M_reduced = M.transpose().LLL(delta=delta).transpose()
            v = check_solution_found(M_reduced, domain)

            #if not found, use BKZ with increasing block_size
            bs=2
            while (v is None):
                bs+=1
                bs_stat.append(bs)
                print("reduce with BKZ and block_size=" + str(bs) + ":")
                M_reduced = M_reduced.transpose().BKZ(block_size=bs).transpose()
                v = check_solution_found(M_reduced, domain)
    
            return (v[-1] * v[0:-1], M, M_reduced)
    
        except TypeError as t:
            print(t)
            return (None, None, None)
    else:
        print("try with " + algorithm)
        try:
            M = matrix(ZZ, B.nrows() + 1, B.ncols() + 1)
            M[:, 0] = vector(tt)
            M[:-1, 1:] = B
            if(algorithm == "LLL"):
                M_reduced = M.transpose().LLL(delta=delta).transpose()
            elif(algorithm == "BKZ"):
                M_reduced = M.transpose().BKZ(delta=delta, block_size=block_size, prune=prune).transpose()
            v = M_reduced.column(0)
            return (v[-1] * v[0:-1], M, M_reduced)
        except(TypeError):
            return (None, None, None)

def babai_round_off(B, t, B1=None):
    if(B1 is None):
        B1 = B.inverse()
    return B*((B1*t).apply_map(lambda x: x.round()).change_ring(ZZ))

def babai_round_off_without_inverse(H, t):
    x = H.solve_right(t)
    return H * x.apply_map(lambda x: x.round()).change_ring(ZZ)

def babai_nearest_plane(B, t, reduced=False):
    n = (B.dimensions())[0]
    if(not reduced):
        G, _ = B.transpose().gram_schmidt()
        G = G.transpose()
    else:
        G = B
    w = t
    v = vector(ZZ, n)
    for i in range(n-1,-1,-1):
        g = G.column(i)
        b = B.column(i)
        l = w.dot_product(g)/g.dot_product(g)
        y = l.round()*b
        w = w - (l - l.round())*g - l.round()*b
        v = v + y
    return v

def random_unimodular(n):
    a = matrix(ZZ, n, sparse=True)
    for i in range(n):
        a[i, i] = 1

    for _ in range(2 * n):
        b = matrix(ZZ, n, sparse=True)
        for i in range(n):
            b[i, i] = 1
        while (true):
            j = random.randint(0, n - 1)
            k = random.randint(0, n - 1)
            if (j != k): break
        b[j, k] = random.randint(1, 4)
        a = a * b
    return a


def random_triangular(n, lower=False):
    l = 4
    A = identity_matrix(n)

    if (lower):
        for i in range(n):
            for j in range(i):
                A[i, j] = random.randint(-l, l)

    else:
        for i in range(n):
            for j in range(i + 1, n):
                A[i, j] = random.randint(-l, l)

    return A

def k_vector_of_length_l(k,l):
    v = vector(ZZ, l)
    for i in range(l):
        v[i] = k
    return v

def remove_doubles(list):
    list.sort()
    rem = []
    for i in range(len(list)-1):
        if(list[i] == list[i+1]):
            rem.append(list[i])
    for r in rem:
        list.remove(r)
    return list

def getAllSolutions(B, y, modulus, ld):
    M = MatrixSpace(Integers(modulus), B.nrows(), B.ncols())
    BB = M(B)
    yy = y.change_ring(Integers(modulus))
    
    #is there a unique solution?
    if(BB.is_invertible()):
        print("B is invertible modulo " + str(modulus))
        res = BB.solve_right(yy)
        ld.define("InverseMod" + numb[modulus], res)
        return [res]

    else:
        if(modulus.is_prime()):
            K = BB.right_kernel_matrix();
            ld.define("KernelMod" + numb[modulus], K)
            try:
                single_solution = BB.solve_right(yy)
                print("single solution modulo " + str(modulus) + ": ")
                print(single_solution)
                ld.define("SingleSolutionMod" + numb[modulus], single_solution)
            except (ValueError):
                return []
            
            solution = []
            for k in K:
                for i in range(modulus):
                    solution.append(k+single_solution)
            ld.define("PossibleSolutionsMod" + numb[modulus], solution)
            return solution
        
        else:
            p = modulus.prime_divisors()[0]
            q = ZZ(modulus / p)
            print("compute modulo " + str(p) + " and " + str(q))
            sol_p = getAllSolutions(B, yy, p, ld)
            sol_q = getAllSolutions(B, yy, q, ld)
            solutions = []
            for s_p in sol_p:
                for s_q in sol_q:
                    for i in range(modulus-1):
                        solutions.append((i+1) * vector(Integers(modulus), CRT_vectors([s_p.change_ring(ZZ), s_q.change_ring(ZZ)], [p,q])))
            return solutions


################################
# GGH-System                   #
################################

class GGH:
    analytics = {}

    def __init__(self, N, sigma=1, algorithm_private="uniform1", algorithm_public="hnf", reducePrivateKey=false, ld=NoneDefiner()):
        self.__N = N
        self.__ld = ld
        self.__ld.define("N", N)
        self.__R = None
        self.algorithm_private = algorithm_private
        self.algorithm_public = algorithm_public
        self.reducePrivateKey = reducePrivateKey
        self.__minimal_sigma = sigma

    @staticmethod
    def FirstAttempt(N, sigma=1, reducePrivateKey=false, ld=NoneDefiner()):
        return GGH(N, sigma=sigma, algorithm_private="uniform", algorithm_public="ggh1", reducePrivateKey=reducePrivateKey, ld=ld)

    @staticmethod
    def GGHParams(N, sigma=1, reducePrivateKey=false, ld=NoneDefiner()):
        return GGH(N, sigma=sigma, algorithm_private="ggh", algorithm_public="ggh1", reducePrivateKey=reducePrivateKey, ld=ld)

    @staticmethod
    def GGHTriagonal(N, sigma=1, reducePrivateKey=false, ld=NoneDefiner()):
        return GGH(N, sigma, algorithm_private="ggh", algorithm_public="triangular", reducePrivateKey=reducePrivateKey, ld=ld)

    @staticmethod
    def Micciancio(N, sigma=1, reducePrivateKey=false, ld=NoneDefiner()):
        return GGH(N, sigma=sigma, algorithm_private="mic", algorithm_public="hnf", reducePrivateKey=reducePrivateKey, ld=ld)

    # REDO:
    def setPrivateKey(self, key):
        C = self.__checkPrivateKey(key)
        R1 = C["R1"]
        s = C["s"]
        
        if(s >= self.__minimal_sigma):
            self.__R = key
            self.__R1 = R1
            self.__s = s

    def getPrivateKey(self):
        return self.__R

    def setPublicKey(self, B):
        self.__B = B
    
    def getPublicKey(self):
        return self.__B
    
    def __checkPrivateKey(self, R):
        if (R == None):
            return {"R1": None, "s": 0}

        if (not R.change_ring(RR).is_invertible()):
            return {"R1": None, "s": 0}
        
        R1 = R.inverse()

        R1norms = []
        # compute norms:
        for i in range(self.__N):
            R1norms.append(float(R1.row(i).norm(1)))
        s = floor(1 / (2 * max(R1norms)));
        printv("s"); printv(s)

        return {"R1": R1, "s": s}

    def __generatePrivateKeyUniform(self):
        R = random_matrix(ZZ, self.__N, self.__N, x=-self.__N, y=self.__N+1);
        return R
    
    def __generatePrivateKeyMic(self):
        G = self.__generatePrivateKeyUniform()
        R = G.transpose().LLL().transpose()
        return R

    def __generatePrivateKeyGGH(self):
        l = 4
        A = random_matrix(ZZ, self.__N, self.__N, x=-l, y=l+1)
        k = floor(sqrt(self.__N) * l)
        B = k * identity_matrix(self.__N)
        R = A + B
        return R

    def __generatePrivateKey(self, algorithm):
        
        if(self.__R is not None):
            return {"R": self.__R, "R1": self.__R1, "s": self.__s, "time": 0, "count" : 0}
        
        R1 = None
        s = 0
        count = 0
        timerPK = Timer("generating private key (method: " + algorithm + ")");
        timerPK.start()
        while (s < self.__minimal_sigma):
            if (algorithm == "uniform"):
                R = self.__generatePrivateKeyUniform()
            elif (algorithm == "ggh"):
                R = self.__generatePrivateKeyGGH()
            elif (algorithm == "mic"):
                R = self.__generatePrivateKeyMic()
            else:
                raise NotImplementedError(algorithm + " not implemented")
            if(self.reducePrivateKey):
                R = R.transpose().LLL().transpose()
            check = self.__checkPrivateKey(R)
            R1 = check["R1"]
            s = check["s"]
            count += 1
        time = timerPK.end()

        return {"R": R, "R1": R1, "s": s, "time": time, "count" : count}

    def __generatePublicKeyUniform(self, R):
        U = random_unimodular(self.__N);
        printv("U");
        printv(U);
        B = R * U;
        printv("B");
        printv(B)
        return B

    def __generatePublicKeyTriangular(self, R):
        B = R
        for _ in range(4):
            U = random_triangular(self.__N);
            printv("U");
            printv(U);
            L = random_triangular(self.__N, True);
            printv("U");
            printv(U);
            B = B * U * L;
            printv("B");
            printv(B)
        return B

    def __generatePublicKeyHNF(self, R):
        B = R.transpose().echelon_form().transpose()
        return B

    def __generatePublicKey(self, R, algorithm):
        result = None
        timerPK = Timer("generating public key (method: " + algorithm + ")");
        timerPK.start()
        if (algorithm == "ggh1"):
            result = self.__generatePublicKeyUniform(R)
        elif (algorithm == "triangular"):
            result = self.__generatePublicKeyTriangular(R)
        elif (algorithm == "hnf"):
            result = self.__generatePublicKeyHNF(R)
        else:
            raise NotImplementedError(algorithm + " not implemented")
        time = timerPK.end()
        return {"B": result, "time": time}

    def keygen(self, inverting=True):
        timerKeyGen = Timer("generating keys");
        timerKeyGen.start()
        timePrivate = 0
        count = 0
        if (self.__R is None):
            privateKeys = self.__generatePrivateKey(self.algorithm_private)
            self.__R = privateKeys["R"]
            self.__R1 = privateKeys["R1"]
            self.__s = privateKeys["s"]
            timePrivate = privateKeys["time"]
            count = privateKeys["count"]
        publicKeys = self.__generatePublicKey(self.__R, self.algorithm_public)
        self.__B = publicKeys["B"]
        timePublic = publicKeys["time"]
        timeTotal = timerKeyGen.end()

        invTime = 0
        if(inverting):
            timerInv = Timer("inverting B");
            timerInv.start()
            self.__B1 = self.__B.inverse()
            invTime = timerInv.end()
            #for faster decryption
            self.__B1R = self.__B1 * self.__R
        
        statistics = {"private": timePrivate, "public": timePublic, "invert": invTime, "total": timeTotal, "iterations" : count, "sigma": self.__s}

        # B1norms = []
        #for i in range(self.__N):
        #    B1norms.append(l1norm(self.__B1[i,:].transpose()))
        #Bs = 1/(2*max(B1norms)); printv("Bs"); printv(Bs)

        self.__ld.define("R", self.__R)
        self.__ld.define("Rinv", self.__R1)
        self.__ld.define("B", self.__B)
        if(inverting):
            self.__ld.define("Binv", self.__B1)
        self.__ld.define("Sigma", self.__s)

        return {"B": self.__B, "s": self.__s, "statistics": statistics}

    def encrypt(self, m, force_sigma=None, t=[]):
        timerEnc = Timer("encrypting");
        timerEnc.start()
        # e1 = random_matrix(ZZ, 1, self.__N, x=-self.__s, y=self.__s+1)
        #e = vector([e1[0,i] for i in range(self.__N)]); printv("e"); printv(e)
        
        if(force_sigma is None):
            sigma = self.__s
        else:
            sigma = force_sigma
        
        e = vector(ZZ, [random.choice([-1, 1]) * sigma for _ in range(self.__N)]);
        printv("e"); printv(e);
        c = self.__B * m + e;
        printv("c");
        printv(c)
        t.append(timerEnc.end())

        self.__ld.define("Message", m)
        self.__ld.define("Error", e)
        self.__ld.define("Ciphertext", c)

        return c

    def decrypt1(self, c, t=[]):
        timerDec = Timer("decrypting");
        timerDec.start()
        x1 = self.__R1 * c
        x = vector([x1[i].round() for i in range(self.__N)]);
        printv("x");
        printv(x)
        m_t = self.__B1R * x
        m = vector([m_t[i].round() for i in range(self.__N)]);
        printv("m1");
        printv(m);
        t.append(timerDec.end())

        self.__ld.define("x", x)
        self.__ld.define("Decryption", m)

        if (LATEX):
            m2 = self.__B1 * c;
            printv("m2");
            printv(m2)
            m2_int = vector([m2[i].round() for i in range(self.__N)]);
            printv("m2_int");
            printv(m2_int)
            self.__ld.define("DecryptionB", m2)
            self.__ld.define("DecryptionBRounded", m2_int)

        return m

    def decrypt(self, c, t=[]):
        timerDec = Timer("decrypting");
        timerDec.start()
        result = (self.__B.solve_right(babai_round_off_without_inverse(self.__R, c))).apply_map(round, ZZ)
        t.append(timerDec.end())
        return result

    def decrypt2(self, c):
        return (self.__B1 * babai_nearest_plane(self.__R, c)).apply_map(lambda x: x.round()).change_ring(ZZ)


################################
# Attack                       #
################################

class GGHAttack:
    analytics = {}

    def __init__(self, N, B, s, ld=NoneDefiner()):
        self.__N = N
        self.__B = B
        self.__BRED = None
        self.__ld = ld
        if(B.is_invertible()):
            self.__B1 = B.inverse()
        self.__s = s

    def embeddingAttack(self, c, algorithm="LLL", delta=None, block_size=10, prune=0, domain=None, reduce_before=false):
        timerAtt = Timer("attacking with basis reduction attack");
        timerAtt.start()
        timeBKZ = 0
        if(reduce_before):
            if(self.__BRED is None):
                timerBKZ = Timer("reduce basis with LLL");
                timerBKZ.start()
                self.__BRED = self.__B.transpose().LLL().transpose()
                timeBKZ = timerBKZ.end()
            (e, M, M_reduced) = CVP2SVP(self.__BRED, c, algorithm=algorithm, delta=delta, block_size=block_size, prune=prune, domain=[-self.__s, self.__s])
        else:
            (e, M, M_reduced) = CVP2SVP(self.__B, c, algorithm=algorithm, delta=delta, block_size=block_size, prune=prune, domain=[-self.__s, self.__s])
        try:
            m = self.__B.solve_right(c-e)
        except(ValueError, TypeError):
            m = vector([])
        timerAtt.end()

        self.__ld.define("EmbeddingAttackM", M)
        self.__ld.define("EmbeddingAttackMLLL", M_reduced)
        self.__ld.define("EmbeddingAttackError", e)

        return m.apply_map(lambda x: x.round()).change_ring(ZZ)
    
    def __compute_m2sigma(self, B, c, sigma, ld=NoneDefiner()):
        return getAllSolutions(B, c, 2*sigma, ld)


    def nguyenAttack(self, c):
        timerAtt = Timer("attacking with Nguyens attack");
        timerAtt.start()

        s = k_vector_of_length_l(self.__s, self.__N)
        cc = (c + s) % (2*self.__s)
        self.__ld.define("NguyenAttackCC", cc)
        m2s_set = remove_doubles(self.__compute_m2sigma(self.__B, cc, self.__s, self.__ld))
        self.__ld.define("NguyenAttackmTwoSset", m2s_set)
        i = 0
        print(m2s_set)
        
        for m2s in m2s_set:
            i += 1
            self.__ld.define("NguyenAttackMTwoS" + numb[i], m2s)
            cprime = (c - self.__B * m2s.change_ring(ZZ)) / (self.__s)
            print(cprime)
            self.__ld.define("NguyenAttackCPrime" + numb[i], cprime)
            (eprime, _, _) = CVP2SVP(2*self.__B, cprime)
            print(eprime)
            if (eprime is None):
                continue
            self.__ld.define("NguyenAttackEPrime" + numb[i], eprime)
            error = eprime * self.__s
            print("error", error)
            self.__ld.define("NguyenAttackError" + numb[i], error)
            
            m =  self.__B.solve_right(c-error)
            
            self.__ld.define("NguyenAttackMhat" + numb[i], m)
            
            ee = c - self.__B*m
            print("ee", ee)
            self.__ld.define("NguyenAttackEE" + numb[i], ee)
            values = {-self.__s, self.__s}

            fail = False
            for e in ee:
                if e not in values:
                    fail = True
            if(not fail):
                timerAtt.end()
                return m
        timerAtt.end()
        return zero_vector(ZZ, self.__N)
    
    def leeAttack(self, c, m_1):
        self.__ld.define("LeeAttackMone", m_1)
        k = len(m_1)
        self.__ld.define("LeeAttackK", k)
        B_1 = self.__B[:,0:k];
        B_2 = self.__B[:,k:self.__B.ncols()]
        self.__ld.define("LeeAttackBone", B_1)
        self.__ld.define("LeeAttackBtwo", B_2)
        
        c_1 = B_1*m_1
        c_2 = c - c_1
        
        self.__ld.define("LeeAttackCone", c_1)
        self.__ld.define("LeeAttackCtwo", c_2)
        
        newCVP = GGHAttack(self.__N, B_2, self.__s)
        m_2 = newCVP.embeddingAttack(c_2);
        
        self.__ld.define("LeeAttackMhat", m_2)
        
        return vector(list(m_1) + list(m_2));
    
    def roundOffAttack(self, c):
        RR = self.__B.transpose().LLL().transpose()
        self.__ld.define("RoundOffAttackRR", RR)
        RR1 = RR.inverse();
        self.__ld.define("RoundOffAttackRRinv", RR1)

        # normal decryption with RR instead of R
        x1 = RR1 * c
        x = vector([x1[i].round() for i in range(self.__N)]);
        self.__ld.define("RoundOffAttackX", x)

        m_t = self.__B.inverse() * RR * x;
        m = vector([m_t[i].round() for i in range(self.__N)]);
        self.__ld.define("RoundOffAttackMhat", m)

        return m
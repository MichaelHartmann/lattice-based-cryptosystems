
#
# *** s03 GGH timings  *** 
#
#
# *** Generate Private Key ***
#
#
try:
	S_generate_private_key
except:
	S_generate_private_key=[]
S_generate_private_key+=[[475, 7485235],]#
#
# *** Generate Public Key ***
#
#
try:
	S_generate_public_key
except:
	S_generate_public_key=[]
S_generate_public_key+=[[475, 35208],]#
#
# *** Encrypt ***
#
#
try:
	S_encrypt
except:
	S_encrypt=[]
S_encrypt+=[[475, 144],[475, 37],[475, 63],[475, 63],[475, 61],[475, 61],[475, 65],[475, 38],[475, 35],[475, 39],]#
#
# *** Decrypt with RoundOff ***
#
#
try:
	S_decrypt_with_roundoff
except:
	S_decrypt_with_roundoff=[]
S_decrypt_with_roundoff+=[[475, 1408],[475, 1318],[475, 2129],[475, 2129],[475, 2130],[475, 2135],[475, 1778],[475, 1325],[475, 1326],[475, 1321],]#
#
# *** success sigma 1 ***
#
#
try:
	S_success_sigma_1
except:
	S_success_sigma_1=[]
S_success_sigma_1+=[[475, 10],]#
#
# *** success sigma 2 ***
#
#
try:
	S_success_sigma_2
except:
	S_success_sigma_2=[]
S_success_sigma_2+=[[475, 10],]#
#
# *** success sigma 4 ***
#
#
try:
	S_success_sigma_4
except:
	S_success_sigma_4=[]
S_success_sigma_4+=[[475, 10],]#
#
# *** success sigma 8 ***
#
#
try:
	S_success_sigma_8
except:
	S_success_sigma_8=[]
S_success_sigma_8+=[[475, 10],]#
#
# *** success sigma 16 ***
#
#
try:
	S_success_sigma_16
except:
	S_success_sigma_16=[]
S_success_sigma_16+=[[475, 0],]#
#
# *** success sigma 1 (with reduced key) ***
#
#
try:
	S_success_sigma_1__with_reduced_key
except:
	S_success_sigma_1__with_reduced_key=[]
S_success_sigma_1__with_reduced_key+=[[475, 10],]#
#
# *** success sigma 2 (with reduced key) ***
#
#
try:
	S_success_sigma_2__with_reduced_key
except:
	S_success_sigma_2__with_reduced_key=[]
S_success_sigma_2__with_reduced_key+=[[475, 10],]#
#
# *** success sigma 4 (with reduced key) ***
#
#
try:
	S_success_sigma_4__with_reduced_key
except:
	S_success_sigma_4__with_reduced_key=[]
S_success_sigma_4__with_reduced_key+=[[475, 10],]#
#
# *** success sigma 8 (with reduced key) ***
#
#
try:
	S_success_sigma_8__with_reduced_key
except:
	S_success_sigma_8__with_reduced_key=[]
S_success_sigma_8__with_reduced_key+=[[475, 10],]#
#
# *** success sigma 16 (with reduced key) ***
#
#
try:
	S_success_sigma_16__with_reduced_key
except:
	S_success_sigma_16__with_reduced_key=[]
S_success_sigma_16__with_reduced_key+=[[475, 0],]
#
# *** s03 GGH sizes  *** 
#
#
# *** Size Private Key ***
#
#
try:
	S_size_private_key
except:
	S_size_private_key=[]
S_size_private_key+=[[475, 126310],]#
#
# *** Size Public Key ***
#
#
try:
	S_size_public_key
except:
	S_size_public_key=[]
S_size_public_key+=[[475, 347141],]#
#
# *** Size Ciphertext ***
#
#
try:
	S_size_ciphertext
except:
	S_size_ciphertext=[]
S_size_ciphertext+=[[475, 41],]
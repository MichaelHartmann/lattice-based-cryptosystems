
#
# *** s03 GGH timings  *** 
#
#
# *** Generate Private Key ***
#
#
try:
	S_generate_private_key
except:
	S_generate_private_key=[]
S_generate_private_key+=[[600, 17028551],]#
#
# *** Generate Public Key ***
#
#
try:
	S_generate_public_key
except:
	S_generate_public_key=[]
S_generate_public_key+=[[600, 63610],]#
#
# *** Encrypt ***
#
#
try:
	S_encrypt
except:
	S_encrypt=[]
S_encrypt+=[[600, 234],[600, 54],[600, 59],[600, 60],[600, 58],[600, 59],[600, 60],[600, 57],[600, 59],[600, 59],]#
#
# *** Decrypt with RoundOff ***
#
#
try:
	S_decrypt_with_roundoff
except:
	S_decrypt_with_roundoff=[]
S_decrypt_with_roundoff+=[[600, 2272],[600, 2177],[600, 2182],[600, 2192],[600, 2245],[600, 2192],[600, 2187],[600, 2176],[600, 2192],[600, 2181],]#
#
# *** success sigma 1 ***
#
#
try:
	S_success_sigma_1
except:
	S_success_sigma_1=[]
S_success_sigma_1+=[[600, 10],]#
#
# *** success sigma 2 ***
#
#
try:
	S_success_sigma_2
except:
	S_success_sigma_2=[]
S_success_sigma_2+=[[600, 10],]#
#
# *** success sigma 4 ***
#
#
try:
	S_success_sigma_4
except:
	S_success_sigma_4=[]
S_success_sigma_4+=[[600, 10],]#
#
# *** success sigma 8 ***
#
#
try:
	S_success_sigma_8
except:
	S_success_sigma_8=[]
S_success_sigma_8+=[[600, 10],]#
#
# *** success sigma 16 ***
#
#
try:
	S_success_sigma_16
except:
	S_success_sigma_16=[]
S_success_sigma_16+=[[600, 0],]#
#
# *** success sigma 1 (with reduced key) ***
#
#
try:
	S_success_sigma_1__with_reduced_key
except:
	S_success_sigma_1__with_reduced_key=[]
S_success_sigma_1__with_reduced_key+=[[600, 10],]#
#
# *** success sigma 2 (with reduced key) ***
#
#
try:
	S_success_sigma_2__with_reduced_key
except:
	S_success_sigma_2__with_reduced_key=[]
S_success_sigma_2__with_reduced_key+=[[600, 10],]#
#
# *** success sigma 4 (with reduced key) ***
#
#
try:
	S_success_sigma_4__with_reduced_key
except:
	S_success_sigma_4__with_reduced_key=[]
S_success_sigma_4__with_reduced_key+=[[600, 10],]#
#
# *** success sigma 8 (with reduced key) ***
#
#
try:
	S_success_sigma_8__with_reduced_key
except:
	S_success_sigma_8__with_reduced_key=[]
S_success_sigma_8__with_reduced_key+=[[600, 10],]#
#
# *** success sigma 16 (with reduced key) ***
#
#
try:
	S_success_sigma_16__with_reduced_key
except:
	S_success_sigma_16__with_reduced_key=[]
S_success_sigma_16__with_reduced_key+=[[600, 0],]
#
# *** s03 GGH sizes  *** 
#
#
# *** Size Private Key ***
#
#
try:
	S_size_private_key
except:
	S_size_private_key=[]
S_size_private_key+=[[600, 211366],]#
#
# *** Size Public Key ***
#
#
try:
	S_size_public_key
except:
	S_size_public_key=[]
S_size_public_key+=[[600, 574524],]#
#
# *** Size Ciphertext ***
#
#
try:
	S_size_ciphertext
except:
	S_size_ciphertext=[]
S_size_ciphertext+=[[600, 41],]
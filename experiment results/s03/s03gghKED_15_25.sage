
#
# *** s03 GGH timings  *** 
#
#
# *** Generate Private Key ***
#
#
try:
	S_generate_private_key
except:
	S_generate_private_key=[]
S_generate_private_key+=[[375, 2618837],[400, 3515669],[425, 4360071],[450, 5874801],[475, 7193619],[500, 7771427],[525, 9302102],]#
#
# *** Generate Public Key ***
#
#
try:
	S_generate_public_key
except:
	S_generate_public_key=[]
S_generate_public_key+=[[375, 19650],[400, 19526],[425, 33658],[450, 40057],[475, 38279],[500, 38456],[525, 44826],]#
#
# *** Encrypt ***
#
#
try:
	S_encrypt
except:
	S_encrypt=[]
S_encrypt+=[[375, 92],[375, 23],[375, 23],[375, 24],[375, 24],[375, 25],[375, 25],[375, 24],[375, 42],[375, 24],[400, 79],[400, 28],[400, 28],[400, 27],[400, 29],[400, 29],[400, 29],[400, 26],[400, 50],[400, 29],[425, 87],[425, 42],[425, 32],[425, 32],[425, 33],[425, 32],[425, 32],[425, 31],[425, 33],[425, 52],[450, 157],[450, 34],[450, 60],[450, 61],[450, 61],[450, 61],[450, 63],[450, 64],[450, 59],[450, 35],[475, 174],[475, 64],[475, 41],[475, 41],[475, 41],[475, 40],[475, 40],[475, 42],[475, 40],[475, 40],[500, 121],[500, 45],[500, 41],[500, 45],[500, 44],[500, 43],[500, 46],[500, 43],[500, 44],[500, 44],[525, 135],[525, 49],[525, 50],[525, 51],[525, 50],[525, 51],[525, 49],[525, 52],[525, 48],[525, 50],]#
#
# *** Decrypt with RoundOff ***
#
#
try:
	S_decrypt_with_roundoff
except:
	S_decrypt_with_roundoff=[]
S_decrypt_with_roundoff+=[[375, 847],[375, 818],[375, 855],[375, 814],[375, 823],[375, 1095],[375, 975],[375, 902],[375, 875],[375, 821],[400, 992],[400, 1082],[400, 924],[400, 925],[400, 933],[400, 933],[400, 949],[400, 926],[400, 1094],[400, 923],[425, 1278],[425, 1785],[425, 1146],[425, 1078],[425, 1077],[425, 1077],[425, 1078],[425, 1100],[425, 1731],[425, 1766],[450, 1199],[450, 1162],[450, 1916],[450, 1917],[450, 1911],[450, 1962],[450, 1965],[450, 1915],[450, 1165],[450, 1173],[475, 2196],[475, 2133],[475, 1309],[475, 1317],[475, 1314],[475, 1313],[475, 1318],[475, 1309],[475, 1310],[475, 1313],[500, 1524],[500, 1475],[500, 1472],[500, 1473],[500, 1481],[500, 1473],[500, 1470],[500, 1471],[500, 1474],[500, 1472],[525, 1733],[525, 1667],[525, 1675],[525, 1684],[525, 1674],[525, 1682],[525, 1677],[525, 1679],[525, 1671],[525, 1675],]#
#
# *** success sigma 1 ***
#
#
try:
	S_success_sigma_1
except:
	S_success_sigma_1=[]
S_success_sigma_1+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 2 ***
#
#
try:
	S_success_sigma_2
except:
	S_success_sigma_2=[]
S_success_sigma_2+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 4 ***
#
#
try:
	S_success_sigma_4
except:
	S_success_sigma_4=[]
S_success_sigma_4+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 8 ***
#
#
try:
	S_success_sigma_8
except:
	S_success_sigma_8=[]
S_success_sigma_8+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 16 ***
#
#
try:
	S_success_sigma_16
except:
	S_success_sigma_16=[]
S_success_sigma_16+=[[375, 0],[400, 0],[425, 0],[450, 0],[475, 0],[500, 0],[525, 0],]#
#
# *** success sigma 1 (with reduced key) ***
#
#
try:
	S_success_sigma_1__with_reduced_key
except:
	S_success_sigma_1__with_reduced_key=[]
S_success_sigma_1__with_reduced_key+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 2 (with reduced key) ***
#
#
try:
	S_success_sigma_2__with_reduced_key
except:
	S_success_sigma_2__with_reduced_key=[]
S_success_sigma_2__with_reduced_key+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 4 (with reduced key) ***
#
#
try:
	S_success_sigma_4__with_reduced_key
except:
	S_success_sigma_4__with_reduced_key=[]
S_success_sigma_4__with_reduced_key+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 8 (with reduced key) ***
#
#
try:
	S_success_sigma_8__with_reduced_key
except:
	S_success_sigma_8__with_reduced_key=[]
S_success_sigma_8__with_reduced_key+=[[375, 10],[400, 10],[425, 10],[450, 10],[475, 10],[500, 10],[525, 10],]#
#
# *** success sigma 16 (with reduced key) ***
#
#
try:
	S_success_sigma_16__with_reduced_key
except:
	S_success_sigma_16__with_reduced_key=[]
S_success_sigma_16__with_reduced_key+=[[375, 0],[400, 0],[425, 0],[450, 0],[475, 0],[500, 0],[525, 0],]
#
# *** s03 GGH sizes  *** 
#
#
# *** Size Private Key ***
#
#
try:
	S_size_private_key
except:
	S_size_private_key=[]
S_size_private_key+=[[375, 79370],[400, 90097],[425, 101264],[450, 113438],[475, 126455],[500, 139664],[525, 153763],]#
#
# *** Size Public Key ***
#
#
try:
	S_size_public_key
except:
	S_size_public_key=[]
S_size_public_key+=[[375, 231263],[400, 254611],[425, 287222],[450, 326325],[475, 354188],[500, 423687],[525, 437006],]#
#
# *** Size Ciphertext ***
#
#
try:
	S_size_ciphertext
except:
	S_size_ciphertext=[]
S_size_ciphertext+=[[375, 41],[400, 41],[425, 41],[450, 41],[475, 41],[500, 41],[525, 41],]
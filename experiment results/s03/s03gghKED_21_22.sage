
#
# *** s03 GGH timings  *** 
#
#
# *** Generate Private Key ***
#
#
try:
	S_generate_private_key
except:
	S_generate_private_key=[]
S_generate_private_key+=[[525, 10939195],]#
#
# *** Generate Public Key ***
#
#
try:
	S_generate_public_key
except:
	S_generate_public_key=[]
S_generate_public_key+=[[525, 45000],]#
#
# *** Encrypt ***
#
#
try:
	S_encrypt
except:
	S_encrypt=[]
S_encrypt+=[[525, 128],[525, 46],[525, 46],[525, 45],[525, 48],[525, 47],[525, 81],[525, 46],[525, 46],[525, 47],]#
#
# *** Decrypt with RoundOff ***
#
#
try:
	S_decrypt_with_roundoff
except:
	S_decrypt_with_roundoff=[]
S_decrypt_with_roundoff+=[[525, 1737],[525, 1689],[525, 2131],[525, 1716],[525, 1674],[525, 2892],[525, 1691],[525, 1685],[525, 1676],[525, 1685],]#
#
# *** success sigma 1 ***
#
#
try:
	S_success_sigma_1
except:
	S_success_sigma_1=[]
S_success_sigma_1+=[[525, 10],]#
#
# *** success sigma 2 ***
#
#
try:
	S_success_sigma_2
except:
	S_success_sigma_2=[]
S_success_sigma_2+=[[525, 10],]#
#
# *** success sigma 4 ***
#
#
try:
	S_success_sigma_4
except:
	S_success_sigma_4=[]
S_success_sigma_4+=[[525, 10],]#
#
# *** success sigma 8 ***
#
#
try:
	S_success_sigma_8
except:
	S_success_sigma_8=[]
S_success_sigma_8+=[[525, 10],]#
#
# *** success sigma 16 ***
#
#
try:
	S_success_sigma_16
except:
	S_success_sigma_16=[]
S_success_sigma_16+=[[525, 0],]#
#
# *** success sigma 1 (with reduced key) ***
#
#
try:
	S_success_sigma_1__with_reduced_key
except:
	S_success_sigma_1__with_reduced_key=[]
S_success_sigma_1__with_reduced_key+=[[525, 10],]#
#
# *** success sigma 2 (with reduced key) ***
#
#
try:
	S_success_sigma_2__with_reduced_key
except:
	S_success_sigma_2__with_reduced_key=[]
S_success_sigma_2__with_reduced_key+=[[525, 10],]#
#
# *** success sigma 4 (with reduced key) ***
#
#
try:
	S_success_sigma_4__with_reduced_key
except:
	S_success_sigma_4__with_reduced_key=[]
S_success_sigma_4__with_reduced_key+=[[525, 10],]#
#
# *** success sigma 8 (with reduced key) ***
#
#
try:
	S_success_sigma_8__with_reduced_key
except:
	S_success_sigma_8__with_reduced_key=[]
S_success_sigma_8__with_reduced_key+=[[525, 10],]#
#
# *** success sigma 16 (with reduced key) ***
#
#
try:
	S_success_sigma_16__with_reduced_key
except:
	S_success_sigma_16__with_reduced_key=[]
S_success_sigma_16__with_reduced_key+=[[525, 0],]
#
# *** s03 GGH sizes  *** 
#
#
# *** Size Private Key ***
#
#
try:
	S_size_private_key
except:
	S_size_private_key=[]
S_size_private_key+=[[525, 153871],]#
#
# *** Size Public Key ***
#
#
try:
	S_size_public_key
except:
	S_size_public_key=[]
S_size_public_key+=[[525, 453272],]#
#
# *** Size Ciphertext ***
#
#
try:
	S_size_ciphertext
except:
	S_size_ciphertext=[]
S_size_ciphertext+=[[525, 41],]
################################################################################################################################
# Statistic 15
#
# - How long does it take to attack the private key in ADCS
# - How good is the approximation of the key (L_2-norm)
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ajtai.sage')

attackTimes = DataSeries("Attack Time")
success = DataSeries("Norm")

def adcs_attack_statistics(dim_start, dim_end, c1=10, c2=10):
    COUNT1 = c1
    COUNT = c2
    
    DIMENSIONS = range(dim_start, dim_end+1)

    tt = 0
    for D in DIMENSIONS:
        r = 0
        for _ in range(COUNT1):
            r += 1
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(D) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
            

            A = ADCS(D)
            pk = A.keygen()

            AT = ADCSAttack(D)
            
            for _ in range(COUNT):
                
                timer = Timer("Attack Time"); timer.start()
                p = AT.nguyenAttack(pk)
                attackTimes.addPoint(D, timer.end())

                success.addPoint(D, min([(A.u+p).norm(2),(A.u-p).norm(2)]))

            tt = timeTotal.end()
        save_statistics("s15ajtNguy_"+str(c1) + "_" + str(c2) +".sage")

def str_statistics():
    text = "#"
    text += "\n# *** s15 ADCS timings  *** \n"
    text += attackTimes.print_series()
    text += success.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type adcs_attack_statistics(start_dim, end_dim, out_counter, enc_counter), out_counter count of iterations of outer loop, enc_counter count of encryptions and decryptions per instance")
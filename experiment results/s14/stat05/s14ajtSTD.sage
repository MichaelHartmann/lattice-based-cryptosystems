################################################################################################################################
# Statistic 05
#
# - How long does it take to generate the private key in NTRU
# - How long does it take to generate the public key in NTRU
# - How long does it take to encrypt a message in NTRU
# - How long does it take to decrypt a message in NTRU
# - Key sizes and Ciphertext size
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ajtai.sage')

privateKeyTimes = DataSeries("Generate Private Key")
publicKeyTimes = DataSeries("Generate Public Key")
encryptTimes = DataSeries("Encrypt")
decryptTimes = DataSeries("Decrypt")
fails = DataSeries("Fails")

privateKeySize = DataSeries("Size Private Key")
publicKeySize = DataSeries("Size Public Key")
cipherTextSize = DataSeries("Size Ciphertext")

def adcs_statistics(dim_start, dim_end, c1=10, c2=10):
    COUNT1 = c1
    COUNT = c2
    
    DIMENSIONS = range(dim_start, dim_end+1)

    tt = 0
    for D in DIMENSIONS:
        r = 0
        for _ in range(COUNT1):
            r += 1
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(D) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
            
            t1 = []
            t2 = []
            A = ADCS(D)
            pk = A.keygen(t1=t1, t2=t2)
            privateKeyTimes.addPoint(D, t1[-1])
            publicKeyTimes.addPoint(D, t2[-1])
            privateKeySize.addPoint(D, getSize(str(A.u)))
            publicKeySize.addPoint(D, getSize(str(list(pk))))
            
            for _ in range(COUNT):
                m = random_vector(ZZ, 100, x=0, y=2)
                timerEnc = Timer("encryption")
                timerEnc.start()
                c = A.encrypt(m)
                encryptTimes.addPoint(D, timerEnc.end())
                timerDec = Timer("decryption")
                timerDec.start()
                d = A.decrypt(c)
                decryptTimes.addPoint(D, timerDec.end())
                
                fails.addPoint(D, (d-m).norm(1))

    
            cipherTextSize.addPoint(D, getSize(str(list(c))))

            tt = timeTotal.end()
        save_statistics("s14ajtSTD_"+str(c1) + "_" + str(c2) +".sage")

def str_statistics():
    text = "#"
    text += "\n# *** s14 ADCS timings  *** \n"
    text += privateKeyTimes.print_series()
    text += publicKeyTimes.print_series()
    text += encryptTimes.print_series()
    text += decryptTimes.print_series()
    text += fails.print_series()
    text += privateKeySize.print_series()
    text += publicKeySize.print_series()
    text += cipherTextSize.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type adcs_statistics(start_dim, end_dim, out_counter, enc_counter), out_counter count of iterations of outer loop, enc_counter count of encryptions and decryptions per instance")
DEBUG = True
PRINT_VARS = False
LATEX = False

load('definitions.sage')

import random as random

try:
    RRR
except:
    RRR = RR.to_prec(256)


################################
# ADCS-specific helpers        #
################################

#Random distribution on the n-dimensional cube of side length r_n = n^n
#denoted as S_n
class Cube:
    def __init__(self, n):
        self.n = n
        self.r_n = n**n
        self.r = self.r_n/2

    def random_element(self):
        l = []
        for i in range(self.n):
            sign = sample([-1,1],1)[0]
            elem = RRR.random_element()*self.r
            l.append(sign*elem)
        v = vector(l)
        return v


#Random distribution on the n-dimensional unit ball.
class UnitBall:
    def __init__(self, n):
        self.n = n

    def random_element(self):
        l = []
        for i in range(self.n):
            sign = sample([-1,1],1)[0]
            elem = RRR.random_element()
            l.append(sign*elem)
        v = vector(RRR,l)

        res = RRR.random_element() * 1/norm(v)*v

        return res


#Random distribution on the n-dimensional ball of radius n^(-c)
#denoted as S_n
class Ball:
    def __init__(self, n):
        self.r = n**(-ADCS.c)
        self.unitball = UnitBall(n)

    def random_element(self):
        return self.r*self.unitball.random_element()


#Radnom distribution of the vectors for the public key in ADCS
#denoted H_u
class H:
    def __init__(self, n, u):
        self.n = n
        self.C = Cube(n)
        self.S = Ball(n)
        self.u = u

    def random_element(self):
        x = self.C.random_element()
        d = x*self.u - round(x*self.u)
        x[self.n-1] -= d/self.u[self.n-1]

        if(round(x*self.u) % 2 == 0):
            odd = false
        else:
            odd = true


        s = zero_vector(RRR, self.n)
        for _ in range(self.n):
            s += self.S.random_element()

        return x + s, odd


def distances(w):
    n = len(w)
    result = [] 
    for i in range(n):
        r = [j for j in range(n)]
        r.remove(i)
        A = column_matrix([w[j] for j in r])
        b = column_matrix(w[i])
        x = (A.transpose() * A).inverse() * A.transpose() * b
        b_bar = A * x
        result.append((b - b_bar).norm(2))    
    return result

def modulo(basis, vector):
    M = column_matrix(basis)
    l = M.inverse() * vector
    l = l.apply_map(floor)
    modulo = vector - (M * l)
    return modulo


def getLatexNormTable(v):
    ncols = 8
    n = 64
    r = "\\begin{tabular}{|rl|rl|rl|rl|}\n\hline\n"
    for i in range(n/4):
        for j in range(4):
            k = (i + j*(n/4))
            r += "$\\norm{\\vect{y_{" + `k+1` + "}}}$: & "
            r += "$" + `round(v[k][1],2)` + "$"
            if (j == 3):
                r += "\\\\\n"
            else:
                r += "& "
    r += "\hline\n\\end{tabular}"
    return r 



################################
# ADCS-System                  #
################################

class ADCS:
    def __init__(self, n, errorless=false, ld=NoneDefiner()):
        self.n = n
        self.m = n**3
        self.r_n = n**n
        self.errorless = errorless
        self.ld = ld

    c = 8

    def __generatePrivateKey(self):
        U = UnitBall(self.n)
        return U.random_element()

    def __generatePublicKey(self, u):
        Hu = H(self.n, u)

        counter = 0
        while (true):
            counter += 1
            lw = [Hu.random_element()[0] for _ in xrange(self.n)]

            dist = distances(lw)
            if(min(dist) > self.r_n/self.n**2):
                self.ld.define("Distances", vector(dist).apply_map(lambda x: round(x,3)))
                self.ld.define("MinDistance", round(min(dist),3))
                break

        lv = []
        oddList = []
        for i in range(self.m):
            random_v, odd = Hu.random_element()
            lv.append(random_v)
            if(odd):
                oddList.append(i)

        return lw, lv, random.choice(oddList)

    def keygen(self, seed=None, t1=[], t2=[]):
        if (seed is not None):
            set_random_seed(seed)
            random.seed(seed)

        time1 = Timer("Generating Private Key"); time1.start()
        self.u = self.__generatePrivateKey()
        t1.append(time1.end())

        time2 = Timer("Generating Public Key"); time2.start()
        self.lw, self.lv, self.index = self.__generatePublicKey(self.u)
        t2.append(time2.end())

        self.ld.define("U", vector(self.u).apply_map(lambda x: round(x,3)))
        self.ld.define("V", matrix(self.lv).apply_map(lambda x: round(x,3)))
        self.ld.define("W", matrix(self.lw).apply_map(lambda x: round(x,3)))
        self.ld.define("Ione", self.index)
        self.ld.define("VectIone", vector(self.lv[self.index]).apply_map(lambda x: round(x,3)))
        self.ld.define("Product", round(self.lv[self.index]*self.u))

        if (self.errorless):
            return self.lw, self.lv, self.index
        else:
            return self.lw, self.lv

    def __enc0(self, seed=None):
        if (seed is not None):
            set_random_seed(seed)
            random.seed(seed)
        b = [choice([0,1]) for _ in xrange(self.m)]
        v = zero_vector(RRR, self.n)
        for i in range(self.m):
            if (b[i] == 1):
                v += self.lv[i]
        return modulo(self.lw, v)

    def __enc1(self):
        r = [RRR.random_element() for _ in xrange(self.n)]
        v = zero_vector(RRR, self.n)
        for i in range(self.n):
            v += r[i]*self.lw[i]
        return v

    def __enc1Errorless(self):
        b = [choice([0,1]) for _ in xrange(self.m)]
        v = (1/2)*self.lv[self.index]
        for i in range(self.m):
            if (b[i] == 1):
                v += self.lv[i]
        return modulo(self.lw, v)

    def encrypt(self, message, seed=None):

        res = []

        for bit in message:
            if (seed is not None):
                seed += 1
            if(bit == 0):
                res.append(self.__enc0(seed=seed))
            elif(bit == 1):
                if (self.errorless):
                    res.append(self.__enc1Errorless())
                else:
                    res.append(self.__enc1())
            else:
                raise ValueError("ADCS supports only 0 and 1 entries in message vector")

        return res

    def decrypt(self, ciphertext):
        d = []
        scalars = []
        for c in ciphertext:
            z = c*self.u
            scalars.append(z)
            if(not self.errorless and abs(z - round(z)) < self.n**(-1)):
                d.append(0)
            elif(self.errorless and abs(z - round(z)) <= 1/4):
                d.append(0)
            else:
                d.append(1)

        e_str = ""
        if(self.errorless):
            e_str = "errorless"
        self.ld.define("Scalar" + e_str, vector(scalars).apply_map(lambda x: round(x,3)))

        return vector(ZZ, d)


################################
# Attack                       #
################################

class ADCSAttack:
    def __init__(self, n, ld=NoneDefiner()):
        self.n = n
        self.m = n**3
        self.ld = ld

    def __generateL(self, beta, lv):
        A = beta * column_matrix(lv)
        M = A.stack(identity_matrix(RRR, self.m))

        return M

    def __computeV(self, X):
        vl = []
        sign = 1
        for i in range(self.m):
            vl.append(sign * X.delete_columns([i]).determinant())
            sign *= -1

        return vector(vl)

    def __computeUs(self, lv, V):
        us = []
        return matrix(lv[1:self.n+1]).inverse()*V[1:self.n+1]
    
    def nguyenAttack(self, public_key):
        lw, lv = public_key
        beta = sqrt(self.n**14 / (2*self.n**7 - 1))
        M = self.__generateL(beta, lv)
        factor = 10e14
        MZ = (factor * M).apply_map(round, ZZ)
        MLLL = MZ.transpose().LLL().transpose()
        R = MLLL/factor
        norms = []
        for i in range(R.ncols()):
            norms.append([i, R.column(i).norm(2)])

        ld.define("NormTable", getLatexNormTable(norms), keep=true)

        usable_vectors = filter(lambda x:x[1] < beta, norms)

        X = []
        for u in usable_vectors:
            X.append(R[self.n:].column(u[0]).apply_map(round, ZZ))

        XM = matrix(ZZ, len(X), self.m, X)

        V = self.__computeV(XM)
        ld.define("Vfull", V)
        ld.define("Vfour", V[:4].apply_map(lambda x: round(x), ZZ))

        us = self.__computeUs(lv, V)
        ld.define("Us", us.apply_map(lambda x: round(x,3)))

        return us


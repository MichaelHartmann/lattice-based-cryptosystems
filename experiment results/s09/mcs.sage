PRINT_VARS = False
load('definitions.sage')

################################
# MCS-specific helpers         #
################################


def CVP2SVP(B, t, algorithm="LLL", delta=None, block_size=10, prune=0, checkFkt=None, neededBlockSize=[]):
    tt = list(t)
    tt.append(1)
    
    M = matrix(ZZ, B.nrows() + 1, B.ncols() + 1)
    M[:, 0] = vector(tt)
    M[:-1, 1:] = B
    
    if(checkFkt is not None):
        (result, MR) = reduce(M.transpose(), checkFkt, neededBlockSize)
        return (result, M, MR.transpose())
    else:
        print(" *** try with " + algorithm + " *** ")
        try:
            if(algorithm == "LLL"):
                M_reduced = M.transpose().LLL(delta=delta).transpose()
            elif(algorithm == "BKZ"):
                M_reduced = M.transpose().BKZ(delta=delta, block_size=block_size, prune=prune).transpose()
            v = M_reduced.column(0)
            return (v[-1] * v[0:-1], M, M_reduced)
        except(TypeError):
            return (None, None, None)


BR = None

#reduces given Basis B with LLL, then with BKZ and increasing blocksize and checks after every step if checkFkt is not None. If so, the reduction stops and returns the result of checkFkt and the reduced basis
def reduce(B, checkFkt, neededBlockSize=[]):
    global BR
    
    print("*** try with LLL: ***")
    B = B.LLL()
    BR = B
    bs = 0
    
    result = checkFkt(B)
    if(result is not None):
        neededBlockSize.append(bs)
        return (result, B)
    
    bs = 2
    while(True):
        bs += 1
        print("*** try with BKZ (block_size=%(bs)s): ***" % locals())
        try:
            B = B.BKZ(block_size=bs)
        except RuntimeError as r:
            print(" *** ERROR while BKZ reduction *** ")
            print(r.message)
            return (None, B)
        BR = B
        result = checkFkt(B)
        if(result is not None):
            neededBlockSize.append(bs)
            return (result, B)
        if(bs > 25):
            neededBlockSize.append(bs)
            return (None, B)





def reducing_hnf_mod_H(H, v):
    n = len(v)
    for j in range(n):
        c = floor(v[j]/H[j,j])
        v = v - c * H.row(j)
    return v

def babai_round_off_without_inverse(H, t):
    x = H.solve_right(t).apply_map(round, R=ZZ, sparse=True)
    return H * x

def babai_nearest_plane(B, t, G = None):
    n = (B.dimensions())[0]
    if(G is None):
        G, _ = B.transpose().gram_schmidt()
        G = G.transpose()
    w = t
    v = vector(ZZ, n)
    for i in range(n-1,-1,-1):
        g = G.column(i)
        b = B.column(i)
        l = w.dot_product(g)/g.dot_product(g)
        lr = round(l)
        y = lr*b
        w = w - (l - lr)*g - lr*b
        v = v + y
    return (v, G)

"""
def backward_substitution(H, y):
    print("H\nc", H)
    print("y", y)
    n = len(y)
    print("n",n)
    x = vector(QQ, n)
    for i in range(n-1,-1,-1):
        print("current i: ", i)
        s = 0
        for j in range(i+1,n):
            print("current j: ", j)
            s += H[i,j] * x[j]
        print("s", s)
        x[i] = (y[i] - s) / H[i,i]
    return x
"""

def compute_s(R, method="inverse"):
    if (R == None):
        return {"R1": None, "s": 0}
        
    if(method == "inverse"):
        if (not R.change_ring(RR).is_invertible()):
            return {"R1": None, "s": 0}
    
        R1 = R.inverse()
        
        R1norms = []
        # compute norms:
        for c in R1.columns():
            R1norms.append(float(c.norm(1)))
        s = floor(1 / (2 * max(R1norms)));

    elif(method == "gram_schmidt"):
        try:
            R1 = R.transpose().gram_schmidt()[0]
        except:
            return {"R1": None, "s": 0}
        else:
            R1norms = []
            for r in R1.rows():
                R1norms.append(float(r.norm()))
            s = floor(1 / 2 * (min(R1norms)));
    else:
        raise NotImplementedError("Method " + method + " not implemented")
    
    return {"s": s, "R1": R1}

################################
# Micciancio CS                #
################################

class MCS:
    analytics = {}

    def __init__(self, N, sigma=1, algorithm_private="uniform1", reducePrivateKey=false, ld=NoneDefiner()):
        self.__N = N
        self.__ld = ld
        self.__ld.define("N", N)
        self.__R = None
        self.__RG = None
        self.algorithm_private = algorithm_private
        self.reducePrivateKey = reducePrivateKey
        self.__minimal_sigma = sigma

    @staticmethod
    def GGHParams(N, sigma=1, reducePrivateKey=false, ld=NoneDefiner()):
        return MCS(N, sigma=sigma, algorithm_private="ggh", reducePrivateKey=reducePrivateKey, ld=ld)

    @staticmethod
    def Micciancio(N, sigma=1, reducePrivateKey=false, ld=NoneDefiner()):
        return MCS(N, sigma=sigma, algorithm_private="mic", reducePrivateKey=reducePrivateKey, ld=ld)

    def getPrivateKey(self):
        return self.__R
    
    def setPrivateKey(self, PK):
        self.__R = PK

    def __generatePrivateKeyUniform(self):
        R = random_matrix(ZZ, self.__N, self.__N, x=-self.__N, y=self.__N+1);
        return R
    
    def __generatePrivateKeyMic(self):
        G = self.__generatePrivateKeyUniform()
        return G

    def __generatePrivateKeyGGH(self):
        l = 4
        A = random_matrix(ZZ, self.__N, self.__N, x=-l, y=l)
        k = floor(sqrt(self.__N) * l)
        B = k * identity_matrix(self.__N)
        R = A + B
        return R

    def __generatePrivateKey(self, algorithm):
        
        if(self.__R is not None):
            return {"R": self.__R, "R1": self.__R1, "s": self.__s, "time": 0, "count" : 0}
        
        R1 = None
        s = 0
        count = 0
        timerPK = Timer("generating private key (method: " + algorithm + ")");
        timerPK.start()
        while (s < self.__minimal_sigma):
            if (algorithm == "uniform"):
                R = self.__generatePrivateKeyUniform()
            elif (algorithm == "ggh"):
                R = self.__generatePrivateKeyGGH()
            elif (algorithm == "mic"):
                R = self.__generatePrivateKeyMic()
            else:
                raise NotImplementedError(algorithm + " not implemented")
            if(self.reducePrivateKey):
                timerLLL = Timer("reducing private basis");
                timerLLL.start()
                R = R.transpose().LLL().transpose()
                timerLLL.end()
            timerBC = Timer("checking private basis");
            timerBC.start()
            #check = compute_s(R)
            check = {"R1": None, "s":2}
            timerBC.end()
            R1 = check["R1"]
            s = check["s"]
            count += 1
        time = timerPK.end()
        printv(count)

        return {"R": R, "R1": R1, "s": s, "time": time, "count" : count}

    def __generatePublicKey(self, R):
        timerPK = Timer("generating public key");
        timerPK.start()
        result = R.transpose().hermite_form()
        time = timerPK.end()
        
        return {"B": result, "time": time}

    def keygen(self):
        timerKeyGen = Timer("generating keys");
        timerKeyGen.start()
        timePrivate = 0
        count = 0
        if (self.__R is None):
            privateKeys = self.__generatePrivateKey(self.algorithm_private)
            self.__R = privateKeys["R"]
            self.__R1 = privateKeys["R1"]
            self.__s = privateKeys["s"]
            timePrivate = privateKeys["time"]
            count = privateKeys["count"]
        else:
            self.__s = compute_s(self.__R, method="gram_schmidt")["s"]
        publicKeys = self.__generatePublicKey(self.__R)
        self.__B = publicKeys["B"]

        timePublic = publicKeys["time"]
        timeTotal = timerKeyGen.end()
        self.__ld.define("S", self.__s)
        statistics = {"private": timePrivate, "public": timePublic, "total": timeTotal, "iterations" : count, "sigma": self.__s}

        self.__ld.define("R", self.__R)
        self.__ld.define("B", self.__B)

        return {"B": self.__B, "s": self.__s, "statistics": statistics}
        
    def encrypt(self, m, t = []):
        self.__ld.define("M", m)
        timerEnc = Timer("encrypting");
        timerEnc.start()

        c = reducing_hnf_mod_H(self.__B, m)

        t.append(timerEnc.end())
        self.__ld.define("C", c)
        return c

    def decrypt(self, c, t = []):
        timerDec = Timer("decrypting");
        timerDec.start()
        
        a = babai_round_off_without_inverse(self.__R, c)
        e = c - a
        
        t.append(timerDec.end())
        self.__ld.define("A", a)
        self.__ld.define("E", e)
        return e

    def decrypt2(self, c, t = []):
        timerDec = Timer("decrypting");
        timerDec.start()
        if(self.__RG is None):
            (v, self.__RG) = babai_nearest_plane(self.__R, c)
        else:
            (v , _) = babai_nearest_plane(self.__R, c, self.__RG)
        t.append(timerDec.end())
        return c-v



################################
# Attack                       #
################################

class MCSAttack:
    analytics = {}
    
    def __init__(self, N, B, s, ld=NoneDefiner()):
        self.__N = N
        self.__B = B
        self.__BRED = None
        self.__ld = ld
        self.__s = s
    
    def embeddingAttack(self, c, t = [], algorithm=None, delta=None, block_size=10, prune=0, domain=None, reduce_before=false, neededBlockSize=[]):
        timerAtt = Timer("attacking with embedding attack");
        timerAtt.start()
        timeBKZ = 0
        
        if (domain is None):
            domain = range(-self.__s, self.__s + 1)
        
        def check_solution_found(B):
            for b in B.rows():
                for e in b[0:-1]:
                    if(e not in domain):
                        break               #jump to outer loop
                else:                       #for...else exists in python. Else block is executed if for does not break
                    return b[-1] * b[:-1]
            return None
        
        if(reduce_before):
            if(self.__BRED is None):
                self.__BRED = self.__B.LLL()
            (e, M, M_reduced) = CVP2SVP(self.__BRED.transpose(), c, algorithm=algorithm, delta=delta, block_size=block_size, prune=prune, checkFkt=check_solution_found, neededBlockSize=neededBlockSize)
        else:
            (e, M, M_reduced) = CVP2SVP(self.__B.transpose(), c, algorithm=algorithm, delta=delta, block_size=block_size, prune=prune, checkFkt=check_solution_found, neededBlockSize=neededBlockSize)
        
        t.append(timerAtt.end())
        
        return e

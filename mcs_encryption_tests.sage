load('mcs.sage')



dim = 3
sigma = 2
m = random_vector(ZZ, dim, x=-sigma, y=sigma+1)


s = MCS.GGHParams(dim, sigma)
timerKG = Timer("generating keys");
timerKG.start()
pk = s.keygen()
print(pk["B"])
print(pk["B"].gram_schmidt())
timerKG.end()

timerE = Timer("encrypting");
timerE.start()
c = s.encrypt(m)
print(c)
print timerE.end()

timerD = Timer("decrypting");
timerD.start()
m1 = s.decrypt(c)
print timerD.end()

print(m == m1)


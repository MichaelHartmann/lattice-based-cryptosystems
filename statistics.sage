load('ggh.sage')

SYSTEMS = [GGH.FirstAttempt, GGH.GGHParams, GGH.GGHTriagonal, GGH.Micciancio]

"""
################################
# KeyGen Test                  #
################################

keygenTimes = []
for System in SYSTEMS:
    timeSeries = {}
    testdimensions = [10*i for i in range(1,11)]
    for n in testdimensions:
        facts = ["private", "public", "total", "invert", "iterations", "sigma"]
        measure = {}
        avg = {}
        for fact in facts:
            measure[fact] = 0
        count = 1
        for _ in range(count):
            system = System(n)
            result = system.keygen(inverting=False)
            times = result["statistics"]
            for fact in facts:
                measure[fact] += times[fact]
        for fact in facts:
            avg[fact] = round(measure[fact] / count)
        timeSeries[n] = avg
    name = system.algorithm_private + " " + system.algorithm_public
    keygenTimes.append({name : timeSeries})

print(keygenTimes)

"""

################################
# Encrypting/Decrypting Test   #
################################

COUNT = 2
SYSTEMS = [GGH.FirstAttempt, GGH.GGHParams, GGH.GGHTriagonal, GGH.Micciancio]
FACTS = ["private", "public", "total", "invert", "iterations", "sigma"]

encdecStatistics = []
testdimensions = [10*i for i in range(1,5)]
for n in testdimensions:
    systems = [System(n) for System in SYSTEMS]
    for system in systems:
        system.keygen()
    measure = {}
    avg = {}
    for _ in range(COUNT):
        m = random_vector(ZZ, dimension)
        for system in systems:
            c = system.encrypt(m)
            m1 = system.decrypt(m)
            if (m != m1):
                print("Error in decryption")
            for fact in FACTS:
                measure[fact] += times[fact]
        for fact in facts:
            avg[fact] = round(measure[fact] / count)
        timeSeries[n] = avg
    name = system.algorithm_private + " " + system.algorithm_public
    encdecStatistics.append({name : timeSeries})


import numpy as np
import random as rnd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

load("ajtai.sage")

A = ADCS(3);
pk = A.keygen()
u = A.u

print(u)

nu = np.linalg.norm(u);

nv = 1/nu;

v = (u[0]/nu**2, u[1]/nu**2, u[2]/nu**2);

print(nv)

xs = []
ys = []
zs = []

xr = []
yr = []
zr = []

for i in range(200):
    a = 10*rnd.random()
    b = 10*rnd.random()
    c = -(u[0] * a + u[1] * b)/u[2]
    xs.append(a)
    ys.append(b)
    zs.append(c)

for i in range(200):
    a = 10*rnd.random()
    b = 10*rnd.random()
    c = (1 - (u[0] * a + u[1] * b))/u[2]
    xs.append(a)
    ys.append(b)
    zs.append(c)

for i in range(200):
    a = 10*rnd.random()
    b = 10*rnd.random()
    c = (2 - (u[0] * a + u[1] * b))/u[2]
    xs.append(a)
    ys.append(b)
    zs.append(c)

for i in [0,1,2]:
    for a in [0,1]:
        for b in [0,1]:
            c = (i - (u[0] * a + u[1] * b))/u[2]
            xr.append(a)
            yr.append(b)
            zr.append(c)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(xs, ys, zs)
for vv in pk[1]:
    ax.scatter(vv[0], vv[1], vv[2], c="g")
ax.scatter(xr, yr, zr, c='r')
ax.scatter([0],[0],[0], c='r')
ax.scatter(v[0],v[1],v[2], c='r')
ax.plot([0, u[0]], [0, u[1]], [0,u[2]])
ax.plot([0, v[0]], [0, v[1]], [0,v[2]])
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

print (xs[10] * u[0] + ys[10] * u[1] + zs[10] *u[2])
print (xs[210] * u[0] + ys[210] * u[1] + zs[210] *u[2])
print (xs[210], ys[210], zs[210])
print (xs[410] * u[0] + ys[410] * u[1] + zs[410] *u[2])

plt.show()
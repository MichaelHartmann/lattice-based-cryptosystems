PRINT_VARS = True
load("ntru.sage")
import pdb

COUNT = 1

print("*** Test Wikipedia example ***")
D = 11
p = 3
q = 32
df = 4
dg = 3
dr = 3

N = NTRU(D, p, q, df, dg, dr)

w_f = N.R([-1,1,1,0,-1,0,1,0,0,1,-1])
w_g = N.R([-1,0,1,1,0,1,0,0,-1,0,-1])

pk = N.keygen(f=w_f, g=w_g)

w_fp = [1,2,0,2,2,1,0,2,1,2]
w_fq = [5,9,6,16,4,15,16,22,20,18,30]

print("KGEN: Test if fp and fq are qongruent to example fp and fq:")
print(N.fp == N.Rp(w_fp))
print(N.fq == N.R(w_fq))

print("KGEN: Test if coefficients of fp and fq are EQUAL to example fp and fq:")
print(N.fp.list() == w_fp)
print(N.fq.list() == w_fq)

w_h = [8,-7,-10,-12,12,-8,15,-13,12,-13,16]

print("KGEN: Test if public key is qongruent to example h:")
print(vector(pk.list()) % N.q == vector(w_h) % N.q)

print("KGEN: Test if public key is EQUAL to example h:")
print(pk.list() == w_h)

w_m = [-1,0,0,1,-1,0,0,0,-1,1,1]
w_r = [-1,0,1,1,1,-1,0,-1]
w_e = [14,11,26,24,14,16,30,7,25,6,19]
e = N.encrypt(pk, w_m, r=w_r)

print("ENC: Test if e is qongruent to example e:")
print(e == N.e)
w_a = [3,-7,-10,-11,10,7,6,7,5,-3,-7]

m2 = N.decrypt(e)
print("DEC: Test if a is qongruent to example a:")
print(N.Rq(N.aq) == N.Rq(w_a))

print("DEC: Test if a is EQUAL to example a:")
print(N.aq == w_a)

print("DEC: Test if m2 is qongruent to m:")
print(N.Rp(m2) == N.Rp(w_m))

print("DEC: Test if m2 is EQUAL to m:")
print(m2 == N.Rp(w_m).list())


COUNT = 1000
D = 23
p = 3
q = 64
df = 10
dg = 7
dr = 5

print("Generic test (with D=%(D)s, p=%(p)s, q=%(q)s, df=%(df)s, dg=%(dg)s, dr=%(dr)s):" % locals())
count = 0

N = NTRU(D, p, q, df, dg, dr)
for _ in range(COUNT):
    pk = N.keygen()
    m = random_vector(ZZ, D, x=-1, y=2).list()
    c = N.encrypt(pk, m)
    m2 = N.decrypt(c)
    if(m2 == N.Rp(m).list()):
        count += 1
    else:
        break
print("Success rate: " + str(count) + " of " + str(COUNT))


COUNT = 1000
D = 107
p = 3
q = 64
df = 15
dg = 12
dr = 5

print("Generic test (Moderate Security):")
count = 0

for _ in range(COUNT):
    N = NTRU(D, p, q, df, dg, dr)
    pk = N.keygen()
    m = random_vector(ZZ, D, x=-1, y=2).list()
    c = N.encrypt(pk, m)
    m2 = N.decrypt(c)
    if(m2 == N.Rp(m).list()):
        count += 1
    else:
        break
print("Success rate: " + str(count) + " of " + str(COUNT))


COUNT = 1000
D = 167
p = 3
q = 128
df = 61
dg = 20
dr = 18

print("Generic test (High Security):")
count = 0

for _ in range(COUNT):
    N = NTRU(D, p, q, df, dg, dr)
    pk = N.keygen()
    m = random_vector(ZZ, D, x=-1, y=2).list()
    c = N.encrypt(pk, m)
    m2 = N.decrypt(c)
    if(m2 == N.Rp(m).list()):
        count += 1
    else:
        break
print("Success rate: " + str(count) + " of " + str(COUNT))


COUNT = 100
D = 503
p = 3
q = 256
df = 216
dg = 72
dr = 55

print("Generic test (Highest Security):")
count = 0

for _ in range(COUNT):
    N = NTRU(D, p, q, df, dg, dr)
    pk = N.keygen()
    m = random_vector(ZZ, D, x=-1, y=2).list()
    c = N.encrypt(pk, m)
    m2 = N.decrypt(c)
    if(m2 == N.Rp(m).list()):
        count += 1
    else:
        break
print("Success rate: " + str(count) + " of " + str(COUNT))


COUNT = 0
D = 23
p = 3
q = 64
df = 10
dg = 7
dr = 5

print("Test Public Key Attack:")
count = 0

N = NTRU(D, p, q, df, dg, dr)
A = NTRUAttack(D, p, q, df, dg, dr)
for _ in range(COUNT):
    pk = N.keygen()
    (v, B) = A.attackPrivateKey(pk)
    if(true): #to define
        count += 1
    else:
        break
print("Success rate: " + str(count) + " of " + str(COUNT))


COUNT = 100
D = 23
p = 3
q = 64
df = 10
dg = 7
dr = 5

print("Test Message Attack:")
count = 0

N = NTRU(D, p, q, df, dg, dr)
A = NTRUAttack(D, p, q, df, dg, dr)
for _ in range(COUNT):
    pk = N.keygen()
    m = random_vector(ZZ, D, x=-1, y=2)
    c = N.encrypt(pk, m.list())
    (m2, B) = A.attackMessage(pk, c)
    if(vector(m2) == m):
        count += 1
    else:
        print(vector(m2))
        print(m)
print("Success rate: " + str(count) + " of " + str(COUNT))



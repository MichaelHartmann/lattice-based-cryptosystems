PRINT_VARS = False
load("ntru.sage")

ld = LatexDefinitions("ntru_examples.txt", "ntru")
COUNT = 1

N = 5; p = 3; q = 16; df = 2; dg = 2; dr = 1

ld.define("N", N); ld.define("P", p); ld.define("Q", q); ld.define("Df", df); ld.define("Dg", dg); ld.define("Dr", dr);

n = NTRU(N, p, q, df, dg, dr)
pk = n.keygen()

ld.define("F", n.f)
ld.define("G", n.g)
ld.define("H", pk)

ld.define("Fv", vector(n.f.list()))
ld.define("Gv", vector(n.g.list()))

ld.define("CF", circulant_matrix(n.f))
ld.define("CG", circulant_matrix(n.g))
ld.define("CH", circulant_matrix(pk))

ld.define("Fp", n.fp)
ld.define("Fq", n.fq)

ld.define("CFp", circulant_matrix(n.fp))
ld.define("CFq", circulant_matrix(n.fq))

m = random_vector(ZZ, N, x=-1, y=2)
ld.define("Message", m)

c = n.encrypt(pk, m.list())
ld.define("R", n.r)
ld.define("C", c)
ld.define("Clist", vector(c.list()))

m2 = n.decrypt(c)
ld.define("A", vector(n.a.list()))
ld.define("Aq", vector(n.aq))
ld.define("Ap", vector(n.ap))
ld.define("Decryption", vector(m2))

A = NTRUAttack(N, p, q, df, dg, dr)
(v, B) = A.attackPrivateKey(pk)
ld.define("PKAttackL", A.l.transpose())
ld.define("PKAttackR", B.transpose())
ld.define("PKAttackV", v)

(v, B) = A.attackMessage(pk, c)
ld.define("MAttackL", A.l.transpose())
ld.define("MAttackR", B.transpose())
ld.define("MAttackV", v)

ld.close()
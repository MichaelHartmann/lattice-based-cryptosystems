load('definitions.sage')

#SYSTEMS = [GGH.FirstAttempt, GGH.GGHParams, GGH.GGHTriagonal, GGH.Micciancio]

def ggh_hnf_statistic():

    C = 10
    result = []

    for N in [5*i for i in range(1, 40)]:
    
        time = 0
    
        for _ in range(C):
            l = 4
            A = random_matrix(ZZ, N, N, x=-l, y=l)
            k = floor(sqrt(N) * l)
            B = k * identity_matrix(N)
            R = A + B

            timer = Timer("generate HNF of R");
            timer.start()
            R.echelon_form()
            t = timer.end()
            time += t

        result.append([N, round(time/C)]);

    print(result)

ggh_hnf_statistic()
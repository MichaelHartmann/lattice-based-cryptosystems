################################################################################################################################
# Statistic 04
#
# - How long does it take to generate the private key in MCS
# - How long does it take to generate the public key in MCS
# - How long does it take to encrypt a message in MCS
# - How long does it take to compute gram-schmidt (<==> first decryption)
# - How long does it take to decrypt a message in MCS with round-off (with LLL reduced basis)
# - How long does it take to decrypt a message in MCS with nearest plane (with LLL reduced basis)
#
################################################################################################################################

import sys

PRINT_VARS = False

load('mcs.sage')

privateKeyTimes = DataSeries("Generate Private Key")
publicKeyTimes = DataSeries("Generate Public Key")
encryptTimes = DataSeries("Encrypt")
gramschmidtTimes = DataSeries("GramSchmidt")
decryptTimes = DataSeries("Decrypt with RoundOff")
decryptTimesNP = DataSeries("Decrypt with Nearest Plane")
fails = DataSeries("Fails")

privateKeySize = DataSeries("Size Private Key")
publicKeySize = DataSeries("Size Public Key")
cipherTextSize = DataSeries("Size Ciphertext")


def mcs_statistics(x=1, y=33, c1=1, c=10):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT1 = c1
    COUNT = c
    
    tt = 0
    for N in DIMENSIONS:
        r = 0
        for _ in range(COUNT1):
            r += 1
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
                
            M = MCS.Micciancio(N)
            k = M.keygen()
            stats = k["statistics"]
                
            privateKeyTimes.addPoint(N, stats["private"])
            publicKeyTimes.addPoint(N, stats["public"])
            
            m = random_vector(ZZ, N, x=-1, y=2)
            gramschmidtT = []
            c = M.encrypt(m)
            d = M.decrypt2(c, t=gramschmidtT)
            gramschmidtTimes.addPoint(N, gramschmidtT[-1])
            
            MS = MatrixSpace(ZZ, N, sparse=True)
            
            privateKeySize.addPoint(N, getSize(M.getPrivateKey().str()))
            publicKeySize.addPoint(N, getSize(str(MS(k["B"]).dict())))
            
            if (d != m):
                fails.addPoint(N, 1)
            
            for i in range(COUNT):
                t0 = []
                t1 = []
                t2 = []
                
                m = random_vector(ZZ, N, x=-1, y=2)
                c = M.encrypt(m, t0)
                
                d1 = M.decrypt(c, t1)
                d2 = M.decrypt2(c, t2)
                
                if (d1 != m):
                    fails.addPoint(N, 2)
                if (d2 != m):
                    fails.addPoint(N, 3)
    
                encryptTimes.addPoint(N, t0[-1])
                decryptTimes.addPoint(N, t1[-1])
                decryptTimesNP.addPoint(N, t2[-1])
    
            cipherTextSize.addPoint(N, getSize(str(c)))

            save_statistics("s04mcsKED_" + str(x) + "_" + str(y) + ".sage")
            tt = timeTotal.end()

def str_statistics():
    text = "#"
    text += "\n# *** s04 MCS timings  *** \n"
    text += privateKeyTimes.print_series()
    text += publicKeyTimes.print_series()
    text += encryptTimes.print_series()
    text += gramschmidtTimes.print_series()
    text += decryptTimes.print_series()
    text += decryptTimesNP.print_series()
    text += fails.print_series()
    text += privateKeySize.print_series()
    text += publicKeySize.print_series()
    text += cipherTextSize.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type mcs_statistics(x, y, c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")
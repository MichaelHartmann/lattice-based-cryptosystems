################################################################################################################################
# Statistic 05
#
# - How long does it take to generate the private key in NTRU
# - How long does it take to generate the public key in NTRU
# - How long does it take to encrypt a message in NTRU
# - How long does it take to decrypt a message in NTRU
# - Key sizes and Ciphertext size
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ntru.sage')

privateKeyTimes = DataSeries("Generate Private Key")
publicKeyTimes = DataSeries("Generate Public Key")
encryptTimes = DataSeries("Encrypt")
decryptTimes = DataSeries("Decrypt")
fails = DataSeries("Fails")

privateKeySize = DataSeries("Size Private Key")
publicKeySize = DataSeries("Size Public Key")
cipherTextSize = DataSeries("Size Ciphertext")

T = [(107, 3, 64, 15, 12, 5),
     (167, 3, 128, 61, 20, 18),
     (263, 3, 128, 50, 24, 16),
     (503, 3, 256, 216, 72, 55),
     (1087, 3, 2048, 120, 362, 20),
     (1171, 3, 2048, 106, 390, 20),
     (1499, 3, 2048, 79, 499, 20)]

def ntru_statistics(c1=10, c2=10):
    COUNT1 = c1
    COUNT = c2
    
    tt = 0
    for t in T:
        r = 0
        for _ in range(COUNT1):
            r += 1
            D = t[0]
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(D) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
            
            t1 = []
            t2 = []
            n = NTRU(*t)
            pk = n.keygen(t1=t1, t2=t2)
            privateKeyTimes.addPoint(D, t1[-1])
            publicKeyTimes.addPoint(D, t2[-1])
            privateKeySize.addPoint(D, getSize(str(n.getPrivateKey())))
            publicKeySize.addPoint(D, getSize(str(pk.list())))
            
            for _ in range(COUNT):
                m = random_vector(ZZ, D, x=-1, y=2).list()
                timerEnc = Timer("encryption", "mu")
                timerEnc.start()
                c = n.encrypt(pk, m)
                encryptTimes.addPoint(D, timerEnc.end())
                timerDec = Timer("decryption", "mu")
                timerDec.start()
                d = n.decrypt(c)
                decryptTimes.addPoint(D, timerDec.end())
                
                if (d != n.Rp(m).list()):
                    fails.addPoint(D, 1)

    
            cipherTextSize.addPoint(D, getSize(str(c.list())))

            tt = timeTotal.end()
        save_statistics("s08ntruSTD_"+str(c1) + "_" + str(c2) +".sage")

def str_statistics():
    text = "#"
    text += "\n# *** s05 NTRU timings  *** \n"
    text += privateKeyTimes.print_series()
    text += publicKeyTimes.print_series()
    text += encryptTimes.print_series()
    text += decryptTimes.print_series()
    text += fails.print_series()
    text += privateKeySize.print_series()
    text += publicKeySize.print_series()
    text += cipherTextSize.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ntru_statistics(c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")
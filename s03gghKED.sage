################################################################################################################################
# Statistic 03
#
# - How long does it take to generate the private key in GGH
# - How long does it take to generate the public key in GGH
# - How long does it take to encrypt a message in GGH
# - How long does it take to decrypt a message in GGH with round-off
# - How large is the success-rate of decrypting message with different sigmas (sigma in {1,2,4,8,16}) (with/without reduced key)
# - Key- and Ciphertext sizes
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ggh.sage')

privateKeyTimes = DataSeries("Generate Private Key")
publicKeyTimes = DataSeries("Generate Public Key")
encryptTimes = DataSeries("Encrypt")
decryptTimes = DataSeries("Decrypt with RoundOff")

success1 = DataSeries("success sigma 1")
success2 = DataSeries("success sigma 2")
success4 = DataSeries("success sigma 4")
success8 = DataSeries("success sigma 8")
success16 = DataSeries("success sigma 16")

success1R = DataSeries("success sigma 1 (with reduced key)")
success2R = DataSeries("success sigma 2 (with reduced key)")
success4R = DataSeries("success sigma 4 (with reduced key)")
success8R = DataSeries("success sigma 8 (with reduced key)")
success16R = DataSeries("success sigma 16 (with reduced key)")

privateKeySize = DataSeries("Size Private Key")
publicKeySize = DataSeries("Size Public Key")
cipherTextSize = DataSeries("Size Ciphertext")


def ggh_statistics(x=1, y=33, c1=1, c=100):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT1 = c1
    COUNT = c
    
    tt = 0
    for N in DIMENSIONS:
        r = 0
        
        suc1 = 0
        suc2 = 0
        suc4 = 0
        suc8 = 0
        suc16 = 0

        suc1R = 0
        suc2R = 0
        suc4R = 0
        suc8R = 0
        suc16R = 0

        for _ in range(COUNT1):
            timeTotal = Timer("Total")
            timeTotal.start()
            r += 1
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
                
            G = GGH.GGHParams(N)
            k = G.keygen(false)
            R = G.getPrivateKey()
            stats = k["statistics"]
            
            G2 = GGH.GGHParams(N)
            RLLL = R.transpose().LLL().transpose()
            G2.setPrivateKey(RLLL)
            G2.setPublicKey(k["B"])
                
            privateKeyTimes.addPoint(N, stats["private"])
            publicKeyTimes.addPoint(N, stats["public"])
            
            for _ in range(COUNT):
            
                m1 = random_vector(ZZ, N, x=-1, y=2)
                m2 = random_vector(ZZ, N, x=-2, y=3)
                m4 = random_vector(ZZ, N, x=-4, y=5)
                m8 = random_vector(ZZ, N, x=-8, y=9)
                m16 = random_vector(ZZ, N, x=-16, y=17)

                t_enc = []
                t_dec = []

                c1 = G.encrypt(m1, 1, t_enc)
                c2 = G.encrypt(m2, 2)
                c4 = G.encrypt(m4, 4)
                c8 = G.encrypt(m8, 8)
                c16 = G.encrypt(m16, 16)
                
                encryptTimes.addPoint(N, t_enc[-1])
                
                d1 = G.decrypt(c1)
                d2 = G.decrypt(c2)
                d4 = G.decrypt(c4)
                d8 = G.decrypt(c8)
                d16 = G.decrypt(c16)
                
                if (d1 == m1):
                    suc1 += 1
                if (d2 == m2):
                    suc2 += 1
                if (d4 == m4):
                    suc4 += 1
                if (d8 == m8):
                    suc8 += 1
                if (d16 == m16):
                    suc16 += 1
    
                d1R = G2.decrypt(c1, t_dec)
                d2R = G2.decrypt(c2)
                d4R = G2.decrypt(c4)
                d8R = G2.decrypt(c8)
                d16R = G2.decrypt(c16)
                    
                decryptTimes.addPoint(N, t_dec[-1])
                    
                if (d1R == m1):
                    suc1R += 1
                if (d2R == m2):
                    suc2R += 1
                if (d4R == m4):
                    suc4R += 1
                if (d8R == m8):
                    suc8R += 1
                if (d16R == m16):
                    suc16R += 1

            save_statistics("s03gghKED_" + str(x) + "_" + str(y) + ".sage")
            tt = timeTotal.end()
    
        success1.addPoint(N, suc1)
        success2.addPoint(N, suc2)
        success4.addPoint(N, suc4)
        success8.addPoint(N, suc8)
        success16.addPoint(N, suc16)
        
        success1R.addPoint(N, suc1R)
        success2R.addPoint(N, suc2R)
        success4R.addPoint(N, suc4R)
        success8R.addPoint(N, suc8R)
        success16R.addPoint(N, suc16R)

        privateKeySize.addPoint(N, getSize(G.getPrivateKey().str()))
        publicKeySize.addPoint(N, getSize(k["B"].str()))
        cipherTextSize.addPoint(N, getSize(str(c)))
        
        save_statistics("s03gghKED_" + str(x) + "_" + str(y) + ".sage")

def str_statistics():
    text = "\n#"
    text += "\n# *** s03 GGH timings  *** \n"
    text += privateKeyTimes.print_series()
    text += publicKeyTimes.print_series()
    text += encryptTimes.print_series()
    text += decryptTimes.print_series()
    
    text += success1.print_series()
    text += success2.print_series()
    text += success4.print_series()
    text += success8.print_series()
    text += success16.print_series()
    
    text += success1R.print_series()
    text += success2R.print_series()
    text += success4R.print_series()
    text += success8R.print_series()
    text += success16R.print_series()
    
    text += "\n#"
    text += "\n# *** s03 GGH sizes  *** \n"
    text += privateKeySize.print_series()
    text += publicKeySize.print_series()
    text += cipherTextSize.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ggh_statistics(x, y, c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")
load('ggh.sage')

SYSTEMS = [GGH.FirstAttempt, GGH.GGHParams, GGH.GGHTriagonal, GGH.Micciancio]

#SYSTEMS = [GGH.GGHParams]


dim = 30
m = random_vector(ZZ, dim, x=-16, y=16)


s = GGH.FirstAttempt(dim, reducePrivateKey=true)
s.keygen()
privateKey = s.getPrivateKey()


"""
for System in SYSTEMS:
    print(System)
    system = System(dim)
    #system.setPrivateKey(privateKey)
    k = system.keygen()
    print("s: " + (k["s"]).str())
    c = system.encrypt(m)
    m1 = system.decrypt(c)
    print(m-m1)
"""

"""
sys = GGH.Micciancio(dim);
k = sys.keygen();
c = sys.encrypt(m)
m1 = sys.decrypt(c)
print(m-m1)
"""

"""
A = random_matrix(ZZ, dim).echelon_form()
A1 = A.inverse()
t = m

timer = Timer("babai1");
timer.start()
v1 = babai_round_off(A, t, A1)
print(timer.end())
print(v1)

timer = Timer("babai2");
timer.start()
v2 = babai_round_off_without_inverse(A, t)
print(timer.end())
print(v2)

print(v1 == v2)

timer = Timer("babai3");
timer.start()
v3 = babai_nearest_plane(A, t)
print(timer.end())
print(v3)

print(v1 == v3)
"""
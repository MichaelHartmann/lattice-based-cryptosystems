load('ggh.sage')

def generate_examples():
    N = 5
    minimal_sigma = 3
    M = matrix(ZZ, N, [11, 3, 0, -4, 1, -2, 9, 2, 1, -1, 3, -2, 10, -1, -1, 1, -4, -2, 11, 0, -3, 1, 1, 0, 11])
    #M = matrix(ZZ, N, [1, -1, 0, 11, 1, 1, 0, -1, 1, 11, 9, 0, 0, 1, -2, -2, 10, 0, 1, 0, -3, 2, 11, -3, 0])
    #M = matrix(ZZ, N, [0, -4, 1, 11, 3 , 9, -1, -3, 0, 0 , 1, 9, 2, 2, -1 , 2, -4, 10, -1, 2 , -1, 0, -2, -2, 11]) #nguyen Attack dont work

    ld = LatexDefinitions("ggh_examples.txt", "ggh")
    
    G = GGH.GGHParams(N, minimal_sigma, True, ld=ld)
    G.setPrivateKey(M)
    K = G.keygen()

    pk = K["B"]
    s = K["s"]

    m = random_vector(ZZ, N, x=-127, y=128)
    
    #decryption only with B
    mB = pk.inverse()*m
    ld.define("DecryptionB", mB)
    ld.define("DecryptionBRounded", mB.apply_map(round))

    c = G.encrypt(m)
    m1 = G.decrypt1(c)

    print("m", m)

    A = GGHAttack(N, pk, s, ld)

    m2 = A.roundOffAttack(c); print("m2", m2)
    m3 = A.embeddingAttack(c); print("m3", m3)
    m4 = A.nguyenAttack(c); print("m4", m4)
    m5 = A.leeAttack(c, m[:1]); print("m5", m5)

    ld.close()

generate_examples()

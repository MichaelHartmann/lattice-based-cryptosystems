################################################################################################################################
# Statistic 05
#
# - How long does it take to generate the private key in NTRU
# - How long does it take to generate the public key in NTRU
# - How long does it take to encrypt a message in NTRU
# - How long does it take to decrypt a message in NTRU
# - Key sizes and Ciphertext size
#
################################################################################################################################

import sys

PRINT_VARS = False

load('ntru.sage')

privateKeyTimes = DataSeries("Generate Private Key")
publicKeyTimes = DataSeries("Generate Public Key")
encryptTimes = DataSeries("Encrypt")
decryptTimes = DataSeries("Decrypt")
fails = DataSeries("Fails")

privateKeySize = DataSeries("Size Private Key")
publicKeySize = DataSeries("Size Public Key")
cipherTextSize = DataSeries("Size Ciphertext")


def ntru_statistics(x=1, y=33, c1=10, c=10):
    
    DIMENSIONS = [i*25 for i in range(x, y)]
    COUNT1 = c1
    COUNT = c
    
    tt = 0
    for N in DIMENSIONS:
        r = 0
        for _ in range(COUNT1):
            r += 1
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()
                
            D = N.next_prime()
            q = 2**(ceil(log(D,2))+1)
            p = 3
            df = round(0.3*D)
            dg = round(0.15*D)
            dr = round(0.05*D)
            
            t1 = []
            t2 = []
            n = NTRU(D, p, q, df, dg, dr)
            pk = n.keygen(t1=t1, t2=t2)
            privateKeyTimes.addPoint(D, t1[-1])
            publicKeyTimes.addPoint(D, t2[-1])
            privateKeySize.addPoint(N, getSize(str(n.getPrivateKey())))
            publicKeySize.addPoint(N, getSize(str(pk.list())))
            
            for _ in range(COUNT):
                m = random_vector(ZZ, D, x=-1, y=2)
                timerEnc = Timer("encryption")
                timerEnc.start()
                c = n.encrypt(pk, m.list())
                encryptTimes.addPoint(D, timerEnc.end())
                timerDec = Timer("decryption")
                timerDec.start()
                d = n.decrypt(c)
                decryptTimes.addPoint(D, timerDec.end())
                
                if (m != vector(d) % 3):
                    fails.addPoint(D, 1)

    
            cipherTextSize.addPoint(D, getSize(str(c.list())))

            save_statistics("s05ntruKED_" + str(x) + "_" + str(y) + ".sage")
            tt = timeTotal.end()

def str_statistics():
    text = "#"
    text += "\n# *** s05 NTRU timings  *** \n"
    text += privateKeyTimes.print_series()
    text += publicKeyTimes.print_series()
    text += encryptTimes.print_series()
    text += decryptTimes.print_series()
    text += fails.print_series()
    text += privateKeySize.print_series()
    text += publicKeySize.print_series()
    text += cipherTextSize.print_series()
    
    return text

def print_statistics():
    print(str_statistics())
    sys.stdout.flush()

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

print("Type ntru_statistics(x, y, c1, c), where x is the start value (times 25), and y-1 is the end value (times 25), c1 count of iterations of outer loop, c count of iterations of inner loop")
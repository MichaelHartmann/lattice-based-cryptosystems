import sys
PRINT_VARS = False

load('ntru.sage')

messageAttackTimeSeries = DataSeries("Time for Message Attack")
messageAttackNeededBlockSize = DataSeries("Needed Block Size")
messageAttackNeededSuccessRate = DataSeries("SucessRate")

DIMENSIONS = [i*25 for i in range(1, 21)]
ROUNDS = 5
SIGMA = 1

def ntru_pk_attack_statistic():
    tt = 0
    for N in DIMENSIONS:
        
        D = N.next_prime()
        q = 2**(ceil(log(D,2))+1)
        p = 3
        df = round(0.3*D)
        dg = round(0.15*D)
        dr = round(0.05*D)
        
        for r in range(ROUNDS):
            timeTotal = Timer("Total")
            timeTotal.start()
            sys.stdout.write("\rDimension: " + str(N) + "\t(round " + str(r+1) + ") \tlast iteration: " + str(round(tt/1000)) + " Sek \t")
            sys.stdout.flush()

            timeMessage = []
            neededBlockSize = []

            n = NTRU(D, p, q, df, dg, dr)
            pk = n.keygen()
            m = random_vector(ZZ, D, x=-1, y=2)
            c = n.encrypt(pk, m.list())
            
            A = NTRUAttack(D, p, q, df, dg, dr)
            (m2, B) = A.attackMessage(pk, c, t=timeMessage, neededBlockSize=neededBlockSize)
            messageAttackTimeSeries.addPoint(D, timeMessage[-1])
            messageAttackNeededBlockSize.addPoint(D, neededBlockSize[-1])

            successRateMessage = 0
            if(m2 is not None and vector(m2) == m):
                successRateMessage += 1

            messageAttackNeededSuccessRate.addPoint(D, successRateMessage)

            save_statistics("s07ntruM.txt")
            tt = timeTotal.end()

            if(ROUNDS*tt > 7*60*60*1000):
                break

def str_statistics():
    text = ""
    text += "\n *** Attack Statistics *** \n"
    text += messageAttackTimeSeries.print_series()
    text += messageAttackNeededBlockSize.print_series()
    text += messageAttackNeededSuccessRate.print_series()
    return text

def save_statistics(filename):
    file = open(filename, "w")
    file.write(str_statistics())
    file.close()

ntru_pk_attack_statistic()
\def \mcsN{ 5 } 
\def \mcsS{ 3 } 
\def \mcsR{ \left(\begin{array}{rrrrr}
11 & 3 & 0 & -4 & 1 \\
-2 & 9 & 2 & 1 & -1 \\
3 & -2 & 10 & -1 & -1 \\
1 & -4 & -2 & 11 & 0 \\
-3 & 1 & 1 & 0 & 11
\end{array}\right) } 
\def \mcsB{ \left(\begin{array}{rrrrr}
1 & 0 & 0 & 1 & 56567 \\
0 & 1 & 0 & 1 & 26966 \\
0 & 0 & 1 & 0 & 15774 \\
0 & 0 & 0 & 2 & 53703 \\
0 & 0 & 0 & 0 & 73269
\end{array}\right) } 
\def \mcsM{ \left(0,\,-3,\,-2,\,-2,\,-1\right) } 
\def \mcsC{ \left(0,\,0,\,0,\,1,\,39176\right) } 
\def \mcsA{ \left(0,\,3,\,2,\,3,\,39177\right) } 
\def \mcsE{ \left(0,\,-3,\,-2,\,-2,\,-1\right) } 

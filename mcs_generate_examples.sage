load('mcs.sage')

M = matrix(ZZ, 5)

def generate_examples():
    while(True):
        N = 5
        minimal_sigma = 3
        M = matrix(ZZ, N, [11, 3, 0, -4, 1, -2, 9, 2, 1, -1, 3, -2, 10, -1, -1, 1, -4, -2, 11, 0, -3, 1, 1, 0, 11])
        #M = matrix(ZZ, N, [0, -4, 1, 11, 3 , 9, -1, -3, 0, 0 , 1, 9, 2, 2, -1 , 2, -4, 10, -1, 2 , -1, 0, -2, -2, 11]) #nguyen Attack dont work

        ld = LatexDefinitions("mcs_examples.txt", "mcs")

        G = MCS.GGHParams(N, minimal_sigma, False, ld=ld)
        G.setPrivateKey(M)
        K = G.keygen()

        pk = K["B"]
        s = K["s"]

        m = random_vector(ZZ, N, x=-s, y=s+1)

        c = G.encrypt(m)
        m1 = G.decrypt(c)

        print(m == m1)

        """
        A = GGHAttack(N, pk, s, ld)

        m2 = A.roundOffAttack(c); print("m2", m2)
        m3 = A.embeddingAttack(c); print("m3", m3)
        m4 = A.nguyenAttack(c); print("m4", m4)
        m5 = A.leeAttack(c, m[:1]); print("m5", m5)
        """
        
        if (c[3] > 0):
            break
                   

        ld.close()


generate_examples()
